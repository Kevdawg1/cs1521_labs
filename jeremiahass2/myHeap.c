// COMP1521 18s1 Assignment 2
// Implementation of heap management system

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "myHeap.h"

// minimum total space for heap
#define MIN_HEAP  4096
// minimum amount of space for a free Chunk (excludes Header)
#define MIN_CHUNK 32

#define ALLOC     0x55555555
#define FREE      0xAAAAAAAA
//
typedef unsigned int uint;   // counters, bit-strings, ...

typedef void *Addr;          // addresses

typedef struct {             // headers for Chunks
   uint  status;             // status (ALLOC or FREE)
   uint  size;               // #bytes, including header
} Header;

static Addr  heapMem;        // space allocated for Heap
static int   heapSize;       // number of bytes in heapMem
static Addr *freeList;       // array of pointers to free chunks
static int   freeElems;      // number of elements in freeList[]
static int   nFree;          // number of free chunks

 // Removes a block from the free list, adjusting nFree as appropriate
static void removeFromFL(void *block);

// initialise heap
int initHeap(int size)
{

   // Round up size to Min heap size, or round up to a multiple of 4
   if (size < MIN_HEAP){
       size = MIN_HEAP;
   } else if (size % 4 != 0){
       size = size + 4 - (size % 4);
   }

   // calloc memory region and set heapMen to first byte
   heapMem = calloc(1,size); // calloc should set heapMem to be all zeroes

   // if calloc fails, exit and return -1
   if (heapMem == NULL) return -1;

   // zero out the region (possibly done by calloc but may as well do again)?
   for (int i = 0; i < size; i++){
        ((char *)heapMem)[i] = 0;
   }




   //allocate freeList array
   freeList = calloc(1,size/MIN_CHUNK*sizeof(Addr));
   // if malloc fails, exit and return -1
   if (freeList == NULL) return -1;
   freeList[0] = heapMem;
   heapSize = size;
   freeElems = size/MIN_CHUNK;
   nFree = 1;

   // setting header of big chunk to FREE
   Header *head = (Header*)heapMem;
   head->status = FREE;
   head->size = size;



   return 0; // this just keeps the compiler quiet
}

// clean heap
void freeHeap()
{
   free(heapMem);
   free(freeList);
}

// allocate a chunk of memory
void *myMalloc(int size)
{

    // Increase size to a multiple of four
    if (size % 4 != 0){
        size = size + 4 - (size % 4);
    }

    if (size < 1) return NULL; // return NULL if size < 1
    // find smallest free chunk that is larger than size + HeaderSize
    int sizeOfSmallest = heapSize + 1; // +1 to ensure that something is found even when the entire heap is free
    Addr smallest = NULL; //
    int posInFreeList = 0;

    // find smallest free chunk that is larger than size + HeaderSize
    for (int i = 0; i < nFree; i++){
        if ((((Header*)freeList[i])->status == FREE) &&
        (((Header*)freeList[i])->size < sizeOfSmallest) &&
        (((Header*)freeList[i])->size >= size + sizeof(Header))){
            sizeOfSmallest = ((Header*)freeList[i])->size;
            smallest = freeList[i];
            posInFreeList = i;
        }
    }


    if (smallest == NULL) return NULL; // search failed



    // if free chunk is smaller than size + headerSize+MIN_CHUNK allocate whole chunk
    if (sizeOfSmallest < size + sizeof(Header) + MIN_CHUNK){
        ((Header*)(smallest))->status = ALLOC;
    } else {     // if free chunk is larger, split into two chunks

        ((Header*)(smallest))->status = ALLOC;
        ((Header*)(smallest))->size = size + sizeof(Header);

        // Updating the header of the next free chunk
        Header *newHead = (Header*)(((char*)smallest) + size + sizeof(Header));
        newHead->status = FREE;
        newHead->size = sizeOfSmallest - (size + sizeof(Header));

        // Updating freeList
        freeList[posInFreeList] = newHead;
    }


    return (smallest + sizeof(Header));


    //
    return NULL; // this just keeps the compiler quiet
}

// free a chunk of memory
void myFree(void *block)
{
    
    if (block == NULL){
        fprintf(stderr, "Attempt to free unallocated chunk\n");
        exit(1);
    } 
   
   // changes block to refer to start of header, rather than start of free block
   char *tmp = (char*)block - sizeof(Header);
   block = (Header*)tmp;
   
      // handle invalid chunks: unallocated chunks, or if address is in middle
    if (((Header*)block)->status != ALLOC){
        fprintf(stderr, "Attempt to free unallocated chunk\n");
        exit(1);
    } else if (((Header*)block)->size > heapSize ){
        fprintf(stderr, "Attempt to free unallocated chunk\n");
        exit(1);
    }
    
    // checks to see if it is in the middle of a chunk (and somehow happened to have valid size and status values wow)
    int j = 0;
    int found = 0;
    Header *toCheck;
    while (j < heapSize){
        toCheck = (Header *)(heapMem + j);
        if (block == (heapMem + j)){
            found = 1;
            break;
        } else {
            j = j + toCheck->size;
        }      
    }
    
    if (found != 1){
        fprintf(stderr, "Attempt to free unallocated chunk\n");
        exit(1);
    }
   

   
   
   // place the chunk into the free list at the appropriate spot
   int indexToInsert = nFree;
   for (int i = nFree - 1; i >= 0; i--){ // finds appropriate spot
       if (block < freeList[i]) indexToInsert = i;
   }
   for (int i = nFree; i > indexToInsert; i--){ // shifts everything along the array
       freeList[i] = freeList[i-1];
   }
   freeList[indexToInsert] = block;

   nFree++; //iterate free counter etc

  // declare head of block as free
  ((Header*)block)->status = FREE;

   // merge free chunks
   // chunk after
   int blockSize = ((Header *)block)->size;
   Addr headAfterAddr = (char*)(block) + blockSize;
   Header *headAfter = (Header*)headAfterAddr;
   if (headAfter->status == FREE){
       removeFromFL(headAfter);
       ((Header*)block)->size = blockSize + ((Header*)headAfter)->size;
   }


   // checking and potentially merging chunk before
   Header *headBefore = NULL;
   for (int i = 0; i < nFree; i++){ // finding address of first free chunk before
       if (freeList[i+1] == block){
           headBefore = (Header*)freeList[i];
           break;
       }
   }
   if (headBefore != NULL){
       int beforeSize = headBefore->size;
       if (((char*)headBefore + beforeSize == block) && (headBefore->status == FREE)){ // if the two free chunks are adjacent, merge
           headBefore->size = ((Header *)block)->size + headBefore->size;
           removeFromFL(block);
       }
   
   }


}

// convert pointer to offset in heapMem
int  heapOffset(void *p)
{
   Addr heapTop = (Addr)((char *)heapMem + heapSize);
   if (p == NULL || p < heapMem || p >= heapTop)
      return -1;
   else
      return p - heapMem;
}

// dump contents of heap (for testing/debugging)
void dumpHeap()
{
   Addr    curr;
   Header *chunk;
   Addr    endHeap = (Addr)((char *)heapMem + heapSize);
   int     onRow = 0;

   curr = heapMem;
   while (curr < endHeap) {
      char stat;
      chunk = (Header *)curr;
      switch (chunk->status) {
      case FREE:  stat = 'F'; break;
      case ALLOC: stat = 'A'; break;
      default:    fprintf(stderr,"Corrupted heap %08x\n",chunk->status); exit(1); break;
      }
      printf("+%05d (%c,%5d) ", heapOffset(curr), stat, chunk->size);
      onRow++;
      if (onRow%5 == 0) printf("\n");
      curr = (Addr)((char *)curr + chunk->size);
   }
   if (onRow > 0) printf("\n");
}

// Removes a block from the free list, adjusting nFree as appropriate
static void removeFromFL(void *block){

    // find block
    int freeListPos = 0;
    for (int i = 0; i < nFree; i++){
        if (freeList[i] == block){
            freeListPos = i;
            break;
        }
    }
    // remove block
    for (int i = freeListPos; i < nFree; i++){
        freeList[i] = freeList[i+1];
    }

    // adjust nFree
    nFree--;
    return;
}
