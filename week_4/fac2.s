# COMP1521 18s2 Week 04 Lab
# Compute factorials, iterative function


### Global data

   .data
msg1:
   .asciiz "n  = "
msg2:
   .asciiz "n! = "
eol:
   .asciiz "\n"

### main() function
   .text
   .globl main
main:
   #  set up stack frame
   sw    $fp, -4($sp)       # push $fp onto stack
   la    $fp, -4($sp)       # set up $fp for this function
   sw    $ra, -4($fp)       # save return address
   sw    $s0, -8($fp)       # save $s0 to use as ... int n;
   addi  $sp, $sp, -12      # reset $sp to last pushed item

   #  code for main()
   li    $s0, 0             # n = 0;
   
   la    $a0, msg1
   li    $v0, 4
   syscall                  # printf("n  = ");

	li 	$v0, 5
	syscall
	move 	$s0, $v0
 
	la    $a0, msg2
	li    $v0, 4
	syscall                  # printf("n!  = ");

	jal 	fac

	move $s0, $v0

	move	$a0, $s0
	li		$v0, 1
	syscall

   la    $a0, eol
   li    $v0, 4
   syscall                  # printf("\n");

   # clean up stack frame
   lw    $s0, -8($fp)       # restore $s0 value
   lw    $ra, -4($fp)       # restore $ra for return
   la    $sp, 4($fp)        # restore $sp (remove stack frame)
   lw    $fp, ($fp)          # restore $fp (remove stack frame)

   li    $v0, 0
   jr    $ra                # return 0

# fac() function

fac:
   # setup stack frame
	sw $fp , -4($sp)
	la $fp, -4($sp)
	sw $ra, -4($fp)
	sw $s0, -8($fp)
	sw $s1, -12($fp)
	addi	$sp, $sp, -16

## ... code for prologue goes here ...

   # code for fac()

	li 	$t0, 1				# i = 1
	li		$s1, 1
	loop:
		bgt 	$t0, $s0, end_loop
		mul	$s1, $s1, $t0
		addi 	$t0, $t0, 1 
		j 		loop

	end_loop:
   	# clean up stack frame
		move	$v0, $s1
		lw 	$s1, -12($fp)
		lw		$s0, -8($fp)
		lw		$ra, -4($fp)
		lw		$fp, ($fp)
		addi	$sp, $sp, 16
		jr		$ra
## ... code for epilogue goes here ...
