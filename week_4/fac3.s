# COMP1521 18s2 Week 04 Lab
# Compute factorials, recursive function


### Global data

   .data
msg1:
   .asciiz "n  = "
msg2:
   .asciiz "n! = "
eol:
   .asciiz "\n"

### main() function
   .text
   .globl main
main:
   #  set up stack frame
   sw    $fp, -4($sp)       # push $fp onto stack
   la    $fp, -4($sp)       # set up $fp for this function
   sw    $ra, -4($fp)       # save return address
   sw    $s0, -8($fp)       # save $s0 to use as ... int n;
   addi  $sp, $sp, -12      # reset $sp to last pushed item

   #  code for main()
   li    $s0, 0             # n = 0;
   
   la    $a0, msg1
   li    $v0, 4
   syscall                  # printf("n  = ");

## ... rest of code for main() goes here ...

	li 	$v0, 5
	syscall
	move 	$a0, $v0 			#a0 is n
	jal 	fac 
	move $s0, $v0

	la    $a0, msg2
	li    $v0, 4
	syscall                  # printf("n!  = ");





	move	$a0, $s0
	li		$v0, 1
	syscall

   la    $a0, eol
   li    $v0, 4
   syscall                  # printf("\n");

   # clean up stack frame
   lw    $s0, -8($fp)       # restore $s0 value
   lw    $ra, -4($fp)       # restore $ra for return
   la    $sp, 4($fp)        # restore $sp (remove stack frame)
   lw    $fp, ($fp)          # restore $fp (remove stack frame)

   li    $v0, 0
   jr    $ra                # return 0

# fac() function

fac:
   # set up stack frame
	sw $fp , -4($sp)
	la $fp, -4($sp)
	sw $ra, -4($fp)
	sw $s0, -8($fp)
	sw $s1, -12($fp)
	addi	$sp, $sp, -16

## ... code for prologue goes here ...



   # code for fac()
	move $s0, $a0
	blt 	$s0, 1, base_case
	beq 	$s0, 1, base_case
	addi	$a0, $a0, -1
	jal fac
	move $s1, $v0
	mul	$v0, $s0, $s1
	j end_fac

	end_fac:
   	# clean up stack frame
		lw 	$s1, -12($fp)
		lw		$s0, -8($fp)
		lw		$ra, -4($fp)
		lw		$fp, ($fp)
		addi	$sp, $sp, 16
		jr		$ra

	base_case:
		# return 1
		li $v0, 1
		j end_fac
## ... code for fac() goes here ...

   # clean up stack frame

## ... code for epilogue goes here ...
