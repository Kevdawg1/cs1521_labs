// where_are_the_bits.c ... determine bit-field order
// COMP1521 Lab 03 Exercise
// Written by ...

#include <stdio.h>
#include <stdlib.h>

struct _bit_fields {
   unsigned int a : 4,
                b : 8,
                c : 20;
};

union _f {
   unsigned int   intval;  // interpret the bits as 1 unsigned int
   struct _bit_fields fields;
};
typedef union _f f;

int main(void)
{
    union _f x;
   x.fields.a = 8;
   x.fields.b = 0;
   x.fields.c = 0;

   printf("intval1: %u\n", x.intval);

   x.fields.a = 0;
   x.fields.b = 0;
   x.fields.c = 1;

   printf("intval2: %u\n", x.intval);

   return 0;
}
