// make_a_float ... read in bit strings to build a float value
// COMP1521 Lab03 exercises
// Written by John Shepherd, August 2017
// Completed by ...

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#define TOP1 0x80
#define TOP2 0xC0
#define TOP3 0xE0
#define TOP4 0xF0

typedef uint32_t Word;

struct _float {
   // define bit_fields for sign, exp and frac
   // obviously they need to be larger than 1-bit each
   // and may need to be defined in a different order
   unsigned int frac:23, exp:8, sign:1;
};
typedef struct _float Float32;

union _bits32 {
   float   fval;  // interpret the bits as a float
   Word    xval;  // interpret as a single 32-bit word
   Float32 bits;  // manipulate individual bits
};
typedef union _bits32 Union32;

void    checkArgs(int, char **);
Union32 getBits(char *, char *, char *);
char   *showBits(Word, char *);
int     justBits(char *, int);

int main(int argc, char **argv)
{
   union _bits32 u;
   char out[50];

   // here's a hint ...
   u.bits.sign = u.bits.exp = u.bits.frac = 0;

   // check command-line args (all strings of 0/1
   // kills program if args are bad
   checkArgs(argc, argv);

   // convert command-line args into components of
   // a Float32 inside a Union32, and return the union
   u = getBits(argv[1], argv[2], argv[3]);

   printf("bits : %s\n", showBits(u.xval,out));
   printf("float: %0.10f\n", u.fval);

   return 0;
}

// convert three bit-strings (already checked)
// into the components of a struct _float
Union32 getBits(char *sign, char *exp, char *frac)
{
   Union32 new;

   // this line is just to keep gcc happy
   // delete it when you have implemented the function
   new.bits.sign = new.bits.exp = new.bits.frac = 0;


	printf("%u\n", new.bits.sign);
	printf("%u\n", new.bits.exp);
	printf("%u\n", new.bits.frac);
	printf("%s\n", sign);
	printf("%s\n", exp);
	printf("%s\n", frac);
   // convert char *sign into a single bit in new.bits
   new.bits.sign = *sign - '0';
   //printf("sign = %u\n", new.bits.sign);

   /*// convert char *exp into an 8-bit value in new.bits
   int i = 0;
   int placevalue = 1;
   while (i < 8){
       new.bits.exp = new.bits.exp + (exp[i] - '0') * placevalue;
       placevalue = placevalue * 2;
       i++;
   }


   // convert char *frac into a 23-bit value in new.bits
   i = 0;
   placevalue = 1;
   while (i < 23){
       new.bits.frac = new.bits.frac + (frac[i] - '0') * placevalue;
       placevalue = placevalue *2;
       i++;
   }
   return new;*/

	int i = -1;
   unsigned int tmp = 1;
   unsigned int newExp = 0;
   while (i <7){
      if(exp[i]-'0' == 1)
      {
         newExp = newExp | tmp;
      }
      newExp = newExp<<1;
       i++;
   }
   if(exp[i]-'0' == 1)
   {
      newExp = newExp | tmp;
   }
   new.bits.exp = newExp;

   // convert char *frac into a 23-bit value in new.bits
   i = -1;
   tmp = 1;
   unsigned int newFrac = 0;
   while (i <22){
      if(frac[i]-'0' == 1)
      {
         newFrac = newFrac | tmp;
      }
      newFrac = newFrac<<1;
       i++;
   }
   if(frac[i]-'0' == 1)
   {
      newFrac = newFrac | tmp;
   }
   new.bits.frac = newFrac;

   return new;

	/*
   int i = 7;
   int placevalue = 128;
   while (i >= 0){
       new.bits.exp = new.bits.exp + (exp[i] - '0') * placevalue;
       placevalue = placevalue /2;
       i--;
   }


   // convert char *frac into a 23-bit value in new.bits
   i = 22;
   placevalue = 4194304;
   while (i >=0){
       new.bits.frac = new.bits.frac + (frac[i] - '0') * placevalue;
       placevalue = placevalue / 2;
       i--;
   }
   return new;
	*/
}

// convert a 32-bit bit-stringin val into a sequence
// of '0' and '1' characters in an array buf
// assume that buf has size > 32
// return a pointer to buf
char *showBits(Word val, char *buf)
{
   // this line is just to keep gcc happy
   // delete it when you have implemented the function
   //buf[0] = '\0';

	/*
   int i = 31;
   while (i >=0){
       int mask =  1;
       int y = val >> i;
       char bit = y & mask;
       buf[i] = bit + '0';
       i--;
   }

   return buf;
	*/
	int i = 0;
	int spaces = 0;
	int temp = 1;

	while (i<=33) {
		char bit;
		switch(33-i) {
			case 1:
			case 10:
				buf[33-i] = ' ';
				spaces++;
				break;
			default:
				bit = (val >> (i-spaces)) & temp;
				buf[33-i] = bit + '0';
				break;
		}
		i++;
	}
	buf[34] = '\0';
	return buf;
}

// checks command-line args
// need at least 3, and all must be strings of 0/1
// never returns if it finds a problem
void checkArgs(int argc, char **argv)
{
   if (argc < 3) {
      printf("Usage: %s Sign Exp Frac\n", argv[0]);
      exit(1);
   }
   if (!justBits(argv[1],1)) {
      printf("%s: invalid Sign: %s\n", argv[0], argv[1]);
      exit(1);
   }
   if (!justBits(argv[2],8)) {
      printf("%s: invalid Exp: %s\n", argv[0], argv[2]);
      exit(1);
   }
   if (!justBits(argv[3],23)) {
      printf("%s: invalid Frac: %s\n", argv[0], argv[3]);
      exit(1);
   }
   return;
}

// check whether a string is all 0/1 and of a given length
int justBits(char *str, int len)
{
   if (strlen(str) != len) return 0;

   while (*str != '\0') {
      if (*str != '0' && *str != '1') return 0;
      str++;
   }
   return 1;
}
