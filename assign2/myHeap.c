// COMP1521 18s1 Assignment 2
// Implementation of heap management system

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "myHeap.h"

// minimum total space for heap
#define MIN_HEAP  4096
// minimum amount of space for a free Chunk (excludes Header)
#define MIN_CHUNK 32

#define ALLOC     0x55555555
#define FREE      0xAAAAAAAA

typedef unsigned int uint;   // counters, bit-strings, ...

typedef void *Addr;          // addresses

typedef struct {             // headers for Chunks
   uint  status;             // status (ALLOC or FREE)
   uint  size;               // #bytes, including header
} Header;

static Addr  heapMem;        // space allocated for Heap
static int   heapSize;       // number of bytes in heapMem
static Addr *freeList;       // array of pointers to free chunks
static int   freeElems;      // number of elements in freeList[]
static int   nFree;          // number of free chunks

// initialise heap
int initHeap(int size)
{
   if (size < MIN_HEAP) {
		size = MIN_HEAP;
	} else if (size % 4 > 0) {
		size = roundUp4(size);
	}
	heapSize = size;
	heapMem = malloc(size);
	nFree = 1;
	freeElems = 1;

	memset(heapMem, 0, heapSize);
	Header *header = (Addr) heapMem;
	header->status = FREE;
	header->size = heapSize;
	freeList = malloc(heapSize / MIN_CHUNK);
	freeList[0] = (Addr)heapMem;

   return 0; // this just keeps the compiler quiet
}

int roundUp4(int num) {
	int remainder = num % 4;
	num += (4-remainder);
	return num;
}

// clean heap
void freeHeap()
{
   free(heapMem);
   free(freeList);
}

// allocate a chunk of memory
void *myMalloc(int size)
{
	if (size < 1) {
		return NULL;
	} else if (size % 4 > 0) {
		size = roundUp4(size);
	}
	
	Header *chunk;
	Header *freeChunk = NULL;
	int alloc_size = size + sizeof(Header);
	for (int i=0; i < freeElems; i++) {
		Addr curr = freeList[i];
		chunk = (Addr)curr;
		if ((freeChunk == NULL || chunk->size < freeChunk->size) && chunk->size >= alloc_size) {
			freeChunk = chunk;
		}
	}

	if (freeChunk == NULL) {
		return NULL;
	}
	if (freeChunk->size > alloc_size + MIN_CHUNK) {
		Header* new = (Addr)freeChunk + alloc_size; // offset from the free available chunk
		new->status = FREE;
		new->size = freeChunk->size - alloc_size;
		freeChunk->size = alloc_size;
		freeChunk->status = ALLOC;
	} else {
		freeChunk->status = ALLOC;
	}

	syncFreeList();

	return ((Addr)freeChunk + sizeof(Header));
}

void syncFreeList() {
	Addr curr;
	Header *chunk;
	Addr    endHeap = (Addr)(heapMem + heapSize);
	int i = 0;
	curr = heapMem;

	while (curr < endHeap) {
		chunk = (Header *)curr;
		if (chunk->status == FREE) {
			freeList[i] = curr;
			i++;
		}
		curr = curr + chunk->size;
	}
	freeElems = i;
	nFree = i;
}

// free a chunk of memory
void myFree(void *block)
{
	if (block == NULL) {
		printf("Attempted to free unidentified chunk\n");
		exit(1);
	}
	Header *chunk = (Addr)block - sizeof(Header);

	if (chunk->status == FREE) {
		printf("Attempted to free unallocated chunk\n");
		exit(1);
	}
	chunk->status = FREE;
	syncFreeList();

	Header *curr;
	Header *next;
	for(int i=0; i < nFree-1; i++) {
		curr = freeList[i];
		next = freeList[i+1];
		if ((Addr)next == (Addr)(curr)+curr->size){
			curr->size = curr->size + next->size;
			freeList[i+1] = curr;
		}
	}
	syncFreeList();
}

void shiftList (int index) {
	while (index < nFree - 1) {
		freeList[index] = freeList[index+1];
		index++;
	}
}

// convert pointer to offset in heapMem
int  heapOffset(void *p)
{
   Addr heapTop = (Addr)((char *)heapMem + heapSize);
   if (p == NULL || p < heapMem || p >= heapTop)
      return -1;
   else
      return p - heapMem;
}

// dump contents of heap (for testing/debugging)
void dumpHeap()
{
   Addr    curr;
   Header *chunk;
   Addr    endHeap = (Addr)((char *)heapMem + heapSize);
   int     onRow = 0;

   curr = heapMem;
   while (curr < endHeap) {
      char stat;
      chunk = (Header *)curr;
      switch (chunk->status) {
      case FREE:  stat = 'F'; break;
      case ALLOC: stat = 'A'; break;
      default:    fprintf(stderr,"Corrupted heap %08x\n",chunk->status); exit(1); break;
      }
      printf("+%05d (%c,%5d) ", heapOffset(curr), stat, chunk->size);
      onRow++;
      if (onRow%5 == 0) printf("\n");
      curr = (Addr)((char *)curr + chunk->size);
   }
   if (onRow > 0) printf("\n");
}
