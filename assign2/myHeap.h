// COMP1521 18s1 Assignment 2
// Interface to heap management system

#ifndef MYHEAP_H
#define MYHEAP_H

// initialise heap
int initHeap(int size);

// clean heap
void freeHeap();

// allocate a chunk of memory
void *myMalloc(int size);

void syncFreeList();

// rounds a number to the nearest multiple of 4
int roundUp4(int num);

// free a chunk of memory
void myFree(void *block);

void shiftList(int index);

// dump contents of heap (for testing/debugging)
void dumpHeap();

// convert pointer to offset in heapMem
int  heapOffset(void *);

#endif
