// COMP1521 17s2 Lab08 ... processes competing for a resource
 
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAXLINE BUFSIZ

void copyInput(char *);

void handler(int sig);

pid_t pid;
pid_t pid2;

int main(void)
{
	int status;
   struct sigaction act;
	struct sigaction challenge;
   memset (&act, 0, sizeof(act));
	act.sa_handler = SIG_IGN;
	challenge.sa_handler = &handler;

	//pid_t parent = getpid();
	
   if ((pid = fork()) != 0) {
		sigaction(SIGINT, &act, NULL);
		copyInput("Parent");
		waitpid(pid, &status, 1);
      //copyInput("Parent");
   }
   else if ((pid2 = fork()) != 0) {
		sigaction(SIGINT, &act, NULL);
		//sigaction(SIGPIPE, &challenge, NULL);
		waitpid(getppid(), &status, 1);
      copyInput("Child");
   }
   else {
		sigaction(SIGINT, &challenge, NULL);
		//sigaction(SIGCHLD, &challenge, NULL);
    	copyInput("Grand-child");
		waitpid(getppid(), &status, 1);
   }
   return 0;
}

void handler(int sig) {
	/*int* status;
	switch (sig) {
		case SIGCHLD:
			waitpid(pid2, status, NULL);
			break;
		case SIGPIPE:
			waitpid(pid, status, NULL);
	}*/
}

void copyInput(char *name)
{
   pid_t mypid = getpid();
   char  line[MAXLINE];
   printf("%s (%d) ready\n", name, mypid);
   while (fgets(line, MAXLINE, stdin) != NULL) {
      printf("%s: %s", name, line);
      //sleep(1);
   }
   printf("%s quitting\n", name);
   return;
}







