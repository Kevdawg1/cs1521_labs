# COMP1521 18s1 Week 05 Lab
#
# void multMatrices(int n, int m, int p,
#                   int A[n][m], int B[m][p], int C[n][p])
# {
#    for (int r = 0; r < n; r++) {
#       for (int c = 0; c < p; c++) {
#          int sum = 0;
#          for (int i = 0; i < m; i++) {
#             sum += A[r][i] * B[i][c];
#          }
#          C[r][c] = sum;
#       }
#    }
# }

   .text
   .globl multMatrices
multMatrices:
   # possible register usage:
   # n is $s0, m is $s1, p is $s2,
   # r is $s3, c is $s4, i is $s5, sum is $s6

	lw 	$t0,8($sp)			# A is $t0
	lw 	$t1,4($sp)			# B is $t1
	lw 	$t2,($sp)			# C is $t2
	
   # set up stack frame for multMatrices()
	sw    $fp, -4($sp)       # push $fp onto stack
   la    $fp, -4($sp)       # set up $fp for this function
   sw    $ra, -4($fp)       # save return address
   sw    $s0, -8($fp)       # save $s0 to use as ... int n;
   sw    $s1, -12($fp)      # save $s1 to use as ... int m;
   sw    $s2, -16($fp)      # save $s2 to use as ... int p;
	sw    $s3, -20($fp)      # save $s3 to use as ... int r;
	sw    $s4, -24($fp)      # save $s4 to use as ... int c;
	sw    $s5, -28($fp)      # save $s5 to use as ... int i;
	sw    $s6, -32($fp)      # save $s6 to use as ... int sum;
	addi  $sp, $sp, -36      # reset $sp to last pushed item
   # implement above C code
	move	$s0, $a0
	move	$s1, $a1
	move	$s2, $a2

	li		$s3, 0		# r = 0
	li 	$t4, 4
	first_loop:
		beq	$s3, $s0, end_first_loop
		li		$s4, 0
			second_loop:
				beq	$s4, $s2, end_second_loop
				li		$s5, 0
				third_loop:
					
					beq	$s5, $s1, end_third_loop
					mul 	$t3, $s3, $s1 		# $t3 is r*m - start of required row
					add 	$t3, $t3, $s5 		
					mul 	$t3, $t3, $t4
					add 	$t3, $t3, $t0		# ($t3) gives A[r][i]
					lw		$t5, ($t3)

					mul 	$t6, $s5, $s2 		# $t6 is i*p - start of required row
					add 	$t6, $t6, $s4 		
					mul 	$t6, $t6, $t4
					add 	$t6, $t6, $t1		# ($t6) gives B[i][c]
					lw		$t7, ($t6)
					mul 	$t5, $t5, $t7
					add	$s6, $s6, $t5

					addi	$s5, $s5, 1
					j		third_loop

				end_third_loop:
					mul	$t3, $s3, $s2				#3, 6, 8, 9
					add 	$t3, $t3, $s4
					mul 	$t3, $t3, $t4
					add 	$t3, $t3, $t2

					sw		$s6, ($t3)
					addi	$s4, $s4, 1
					li		$s6, 0
					j 		second_loop
		end_second_loop:
			addi	$s3, $s3, 1
			j		first_loop
		
	end_first_loop:	
   # clean up stack and return
		lw    $s6, -32($fp)      # restore $s6 value
		lw    $s5, -28($fp)      # restore $s5 value
		lw    $s4, -24($fp)      # restore $s4 value
		lw    $s3, -20($fp)      # restore $s3 value
		lw    $s2, -16($fp)      # restore $s2 value
		lw    $s1, -12($fp)      # restore $s1 value
		lw    $s0, -8($fp)       # restore $s0 value
		lw    $ra, -4($fp)       # restore $ra for return
		la    $sp, 4($fp)        # restore $sp (remove stack frame)
		lw    $fp, ($fp)         # restore $fp (remove stack frame)
		jr		$ra








