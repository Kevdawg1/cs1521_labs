// BigNum.h ... LARGE positive integer values

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>
#include "BigNum.h"

// Initialise a BigNum to N bytes, all zero
void initBigNum(BigNum *n, int Nbytes)
{
   n->bytes = malloc(Nbytes * sizeof(Nbytes));
	n->nbytes = Nbytes;
   return;
}

// Add two BigNums and store result in a third BigNum
void addBigNums(BigNum n, BigNum m, BigNum *res)
{
	int size;
	if (n.nbytes > m.nbytes){
		size = n.nbytes + 1;
		res->nbytes = n.nbytes+1;
		res->bytes = realloc(res->bytes,m.nbytes+1);
	} else {
		size = m.nbytes +1;
		res->nbytes = m.nbytes+1;
		res->bytes = realloc(res->bytes,m.nbytes +1);
	}
	int i = 0;
	while (i < res->nbytes) {
		res->bytes[i] = 0;
		i++;
	}
	i = 0;
	char addN = '0';
	char addM = '0';
	while (i < size) {
		if (i >= n.nbytes) addN = '0';
		else addN = n.bytes[i];
		//printf("addN: %c\n",addN);

		if (i >= m.nbytes) addM = '0';
		else addM = m.bytes[i];
		//printf("addM: %c\n",addM);

		res->bytes[i] += addN + addM - '0';
		//printf("res: %c\n\n",res->bytes[i]);
		if (res->bytes[i] > '9') {
			res->bytes[i] = res->bytes[i] - 10;
			res->bytes[i+1]++;
		}
		i++;
	}
   return;
}

// Set the value of a BigNum from a string of digits
// Returns 1 if it *was* a string of digits, 0 otherwise
int scanBigNum(char *s, BigNum *n)
{
   int i = 0;
	int start = -1;
	int end = -1;
	while (s[i] != '\0'){
		if ((s[i] >= '0') && (s[i] <= '9')){
			start = i;
			while ((s[i] != '\0') && (s[i] >= '0') && (s[i] <= '9')){
				i++;
			}
			i--;
			end = i;
			break;
		}
		i++;
	}


	n->nbytes = end - start + 1;
	n->bytes = realloc(n->bytes, n->nbytes);

	i = 0;
	while (end - i >= start){
		n->bytes[i] = s[end-i];
		i++;
	}


	if (end != -1) return 1;
	else return 0;

}

// Display a BigNum in decimal format
void showBigNum(BigNum n)
{
   int i = n.nbytes - 1;
	while ((i >= 0) && (n.bytes[i] == '0')){
		i--;
	}

	while (i >= 0){
		printf("%c", n.bytes[i]);
		i--;
	}
   return;
}

void subtractBigNums (BigNum n, BigNum m, BigNum *res){
    // for negative handling, figure out which is bigger, if m > n
    // swap m and n, then negative the result at the end
    int size;
	if (n.nbytes > m.nbytes){
		size = n.nbytes + 1;
		res->nbytes = n.nbytes+1;
		res->bytes = realloc(res->bytes,m.nbytes+1);
	} else {
		size = m.nbytes +1;
		res->nbytes = m.nbytes+1;
		res->bytes = realloc(res->bytes,m.nbytes +1);
	}
	int i = 0;
	while (i < res->nbytes) {
		res->bytes[i] = 0;
		i++;
	}
	i = 0;
	char addN = '0';
	char addM = '0';
	while (i < size) {
		if (i >= n.nbytes) addN = '0';
		else addN = n.bytes[i];
		//printf("addN: %c\n",addN);

		if (i >= m.nbytes) addM = '0';
		else addM = m.bytes[i];
		//printf("addM: %c\n",addM);

		res->bytes[i] = res->bytes[i] + addN - addM +'0';
		//printf("res: %c\n\n",res->bytes[i]);
		if (res->bytes[i] < '0') {
			res->bytes[i] = res->bytes[i] + 10;
			res->bytes[i+1]--;
		}
		i++;
	}
   return;
}

void multiplyBigNums (BigNum n, BigNum m, BigNum *res){
    int size;
    size = n.nbytes + m.nbytes;
    res->nbytes = size;
    res->bytes = realloc(res->bytes,size);

	int i = 0;
	while (i < res->nbytes) {
		res->bytes[i] = 0;
		i++;
	}
	i = 0;
	char multN = 0;
	char multM = 0;

    int pos = 0; //records position of each row (follow mult algo)
    while (pos < m.nbytes){
        i = 0;
        while (i < n.nbytes) {
    		//if (i >= n.nbytes) multN = 0;
    		multN = n.bytes[i]-'0';
    		//printf("multN: %c\n",multN);

    		//if (pos >= m.nbytes) multM = 0;
    		multM = m.bytes[pos]-'0';
    		//printf("multM: %c\n",multM);

    		res->bytes[i+pos] = (res->bytes[i+pos]) + multN * multM;

            char mod;
            mod = res->bytes[i+pos] % 10;
            res->bytes[i+pos+1] = (res->bytes[i+pos] - mod)/10;
            res->bytes[i+pos] = mod;

    		/*while (res->bytes[i+pos] > '9') {
    			res->bytes[i+pos] = res->bytes[i+pos] - 10;
    			res->bytes[i+1+pos]++;
                printf("enterd while\n");
    		}*/
            //printf("res: %c\n\n",(res->bytes[i+pos] + '0'));
    		i++;
    	}
        pos++;
    }
    i = 0;
    while (i < res->nbytes) {
		res->bytes[i] = res->bytes[i] + '0';
		i++;
	}
   return;
}
