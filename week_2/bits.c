// COMP1521 18s1 Week 02 Lab (warm-up)

#include <stdio.h>

int main()
{
	// Code to generate and display the largest "int" value

	int x = 1; // 2 ^ 31 - 1
	x = (unsigned int)x << (sizeof(int)*8 -1);
	x = ~x;
	printf("int %x, %d\n", x, x);

	// Code to generate and display the largest "unsigned int" value

	unsigned int y = 0;
	y= ~y;
	printf("unsigned int %x, %u\n", y, y);

	// Code to generate and display the largest "long int" value

	long int xx = x;
	printf("long int %lx, %ld\n", xx, xx);

	// Code to generate and display the largest "unsigned long int" value

	unsigned long int xy = y;
	printf("unsigned long int %lx, %lu\n", xy, xy);

	// Code to generate and display the largest "long long int" value

	long long int xxx = 1;
	//long long int mask = 1;
	xxx = (unsigned long long int)xxx << (sizeof(long long int)*8 -1);
	xxx = ~xxx;

	printf("long long int %llx, %lld\n", xxx, xxx);

	// Code to generate and display the largest "unsigned long long int" value

	unsigned long long int xxy = 0;
	xxy = ~xxy;
	printf("unsigned long long int %llx, %llu\n", xxy, xxy);

	return 0;
}

