#
# COMP1521 18s1 -- Assignment 1 -- Worm on a Plane!
#
# Base code by Jashank Jeremy and Wael Alghamdi
# Tweaked (severely) by John Shepherd
#
# Set your tabstop to 8 to make the formatting decent

# Requires:
#  - [no external symbols]

# Provides:
	.globl	wormCol
	.globl	wormRow
	.globl	grid
	.globl	randSeed

	.globl	main
	.globl	clearGrid
	.globl	drawGrid
	.globl	initWorm
	.globl	onGrid
	.globl	overlaps
	.globl	moveWorm
	.globl	addWormToGrid
	.globl	giveUp
	.globl	intValue
	.globl	delay
	.globl	seedRand
	.globl	randValue

	# Let me use $at, please.
	.set	noat

# The following notation is used to suggest places in
# the program, where you might like to add debugging code
#
# If you see e.g. putc('a'), replace by the three lines
# below, with each x replaced by 'a'
#
# print out a single character
# define putc(x)
# 	addi	$a0, $0, x
# 	addiu	$v0, $0, 11
# 	syscall
# 
# print out a word-sized int
# define putw(x)
# 	add 	$a0, $0, x
# 	addiu	$v0, $0, 1
# 	syscall

####################################
# .DATA
	.data

	.align 4
wormCol:	.space	40 * 4
	.align 4
wormRow:	.space	40 * 4
	.align 4
grid:		.space	20 * 40 * 1

randSeed:	.word	0

dot:		.ascii	"."
newLine:	.ascii	"\n"
oChar:	.ascii	"o"
atSign:	.ascii	"@"

main__0:	.asciiz "Invalid Length (4..20)"
main__1:	.asciiz "Invalid # Moves (0..99)"
main__2:	.asciiz "Invalid Rand Seed (0..Big)"
main__3:	.asciiz "Iteration "
main__4:	.asciiz "Blocked!\n"

	# ANSI escape sequence for 'clear-screen'
main__clear:	.asciiz "\033[H\033[2J"
# main__clear:	.asciiz "__showpage__\n" # for debugging

giveUp__0:	.asciiz "Usage: "
giveUp__1:	.asciiz " Length #Moves Seed\n"

####################################
# .TEXT <main>
	.text
main:

# Frame:	$fp, $ra, $s0, $s1, $s2, $s3, $s4
# Uses: 	$a0, $a1, $v0, $s0, $s1, $s2, $s3, $s4
# Clobbers:	$a0, $a1

# Locals:
#	- `argc' in $s0
#	- `argv' in $s1
#	- `length' in $s2
#	- `ntimes' in $s3
#	- `i' in $s4

# Structure:
#	main
#	-> [prologue]
#	-> main_seed
#	  -> main_seed_t
#	  -> main_seed_end
#	-> main_seed_phi
#	-> main_i_init
#	-> main_i_cond
#	   -> main_i_step
#	-> main_i_end
#	-> [epilogue]
#	-> main_giveup_0
#	 | main_giveup_1
#	 | main_giveup_2
#	 | main_giveup_3
#	   -> main_giveup_common

# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	sw	$ra, -8($sp)
	sw	$s0, -12($sp)
	sw	$s1, -16($sp)
	sw	$s2, -20($sp)
	sw	$s3, -24($sp)
	sw	$s4, -28($sp)
	la	$fp, -4($sp)
	addiu	$sp, $sp, -28

	# save argc, argv
	add	$s0, $0, $a0
	add	$s1, $0, $a1

	# if (argc < 3) giveUp(argv[0],NULL);
	slti	$at, $s0, 4
	bne	$at, $0, main_giveup_0

	# length = intValue(argv[1]);
	addi	$a0, $s1, 4	# 1 * sizeof(word)
	lw	$a0, ($a0)	# (char *)$a0 = *(char **)$a0
	jal	intValue

	# if (length < 4 || length >= 40)
	#     giveUp(argv[0], "Invalid Length");
	# $at <- (length < 4) ? 1 : 0
	slti	$at, $v0, 4
	bne	$at, $0, main_giveup_1
	# $at <- (length < 40) ? 1 : 0
	slti	$at, $v0, 40
	beq	$at, $0, main_giveup_1
	# ... okay, save length
	add	$s2, $0, $v0

	# ntimes = intValue(argv[2]);
	addi	$a0, $s1, 8	# 2 * sizeof(word)
	lw	$a0, ($a0)
	jal	intValue

	# if (ntimes < 0 || ntimes >= 100)
	#     giveUp(argv[0], "Invalid # Iterations");
	# $at <- (ntimes < 0) ? 1 : 0
	slti	$at, $v0, 0
	bne	$at, $0, main_giveup_2
	# $at <- (ntimes < 100) ? 1 : 0
	slti	$at, $v0, 100
	beq	$at, $0, main_giveup_2
	# ... okay, save ntimes
	add	$s3, $0, $v0

main_seed:
	# seed = intValue(argv[3]);
	add	$a0, $s1, 12	# 3 * sizeof(word)
	lw	$a0, ($a0)
	jal	intValue

	# if (seed < 0) giveUp(argv[0], "Invalid Rand Seed");
	# $at <- (seed < 0) ? 1 : 0
	slt	$at, $v0, $0
	bne	$at, $0, main_giveup_3

main_seed_phi:
	add	$a0, $0, $v0
	jal	seedRand

	# start worm roughly in middle of grid

	# startCol: initial X-coord of head (X = column)
	# int startCol = 40/2 - length/2;
	addi	$s4, $0, 2
	addi	$a0, $0, 40
	div	$a0, $s4
	mflo	$a0
	# length/2
	div	$s2, $s4
	mflo	$s4
	# 40/2 - length/2
	sub	$a0, $a0, $s4

	# startRow: initial Y-coord of head (Y = row)
	# startRow = 20/2;
	addi	$s4, $0, 2
	addi	$a1, $0, 20
	div	$a1, $s4
	mflo	$a1

	# initWorm($a0=startCol, $a1=startRow, $a2=length)
	add	$a2, $0, $s2
	jal	initWorm

main_i_init:
	# int i = 0;
	add	$s4, $0, $0
main_i_cond:
	# i <= ntimes  ->  ntimes >= i  ->  !(ntimes < i)
	#   ->  $at <- (ntimes < i) ? 1 : 0
	slt	$at, $s3, $s4
	bne	$at, $0, main_i_end

	# clearGrid();
	jal	clearGrid

	# addWormToGrid($a0=length);
	add	$a0, $0, $s2
	jal	addWormToGrid

	# printf(CLEAR)
	la	$a0, main__clear
	addiu	$v0, $0, 4	# print_string
	syscall

	# printf("Iteration ")
	la	$a0, main__3
	addiu	$v0, $0, 4	# print_string
	syscall

	# printf("%d",i)
	add	$a0, $0, $s4
	addiu	$v0, $0, 1	# print_int
	syscall

	# putchar('\n')
	addi	$a0, $0, 0x0a
	addiu	$v0, $0, 11	# print_char
	syscall

	# drawGrid();
	jal	drawGrid

	# Debugging? print worm pos as (r1,c1) (r2,c2) ...

	# if (!moveWorm(length)) {...break}
	add	$a0, $0, $s2
	jal	moveWorm
	bne	$v0, $0, main_moveWorm_phi

	# printf("Blocked!\n")
	la	$a0, main__4
	addiu	$v0, $0, 4	# print_string
	syscall

	# break;
	j	main_i_end

main_moveWorm_phi:
	addi	$a0, $0, 1
	jal	delay

main_i_step:
	addi	$s4, $s4, 1
	j	main_i_cond
main_i_end:

	# exit (EXIT_SUCCESS)
	# ... let's return from main with `EXIT_SUCCESS' instead.
	addi	$v0, $0, 0	# EXIT_SUCCESS

main__post:
	# tear down stack frame
	lw	$s4, -24($fp)
	lw	$s3, -20($fp)
	lw	$s2, -16($fp)
	lw	$s1, -12($fp)
	lw	$s0, -8($fp)
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)
	jr	$ra #$4, $0, $18    

main_giveup_0:
	add	$a1, $0, $0	# NULL
	j	main_giveup_common
main_giveup_1:
	la	$a1, main__0	# "Invalid Length"
	j	main_giveup_common
main_giveup_2:
	la	$a1, main__1	# "Invalid # Iterations"
	j	main_giveup_common
main_giveup_3:
	la	$a1, main__2	# "Invalid Rand Seed"
	# fall through
main_giveup_common:
	# giveUp ($a0=argv[0], $a1)l
	lw	$a0, ($s1)	# argv[0]
	jal	giveUp		# never returns

####################################
# clearGrid() ... set all grid[][] elements to '.'
# .TEXT <clearGrid>
	.text
clearGrid:

# Frame:	$fp, $ra, $s0, $s1
# Uses: 	$s0, $s1, $t1, $t2
# Clobbers:	$t1, $t2

# Locals:
#	- `row' in $s0
#	- `col' in $s1
#	- `&grid[row][col]' in $t1
#	- '.' in $t2

# Code:
	# set up stack frame
	sw		$fp, -4($sp)
	la		$fp, -4($sp)
	sw		$ra, -4($fp)
	sw		$s0, -8($fp)
	sw		$s1, -12($fp)

	addi	$sp, $sp, -16

	la		$t1, grid	# load grid[row][col]
	lb		$t2, dot		# load dot = '.'
	li		$t4, 1		# sizeOfByte = 1
	li 	$s0, 0		# int row = 0

	clearRowLoop:
		beq	$s0, 20, endClearRowLoop				# if row == nRows
		li		$s1, 0										# int col = 0
		clearColLoop:
			beq	$s1, 40, endClearColLoop			# if col == nCols
			mul	$t0, $s0, 40							# int index = row * nCols
			add	$t0, $t0, $s1							# index += col
			mul	$t0, $t0, $t4							# index *= sizeOfInt
			add	$t0, $t0, $t1							# index += grid[0][0]
			sb		$t2, ($t0)								# grid[row][col] = '.'
			addi	$s1, $s1, 1								# col++
			j		clearColLoop
		endClearColLoop:
			addi 	$s0, $s0, 1								# row++
			j		clearRowLoop
	endClearRowLoop:

### TODO: Your code goes here

	# tear down stack frame
	lw		$s1, -12($fp)
	lw		$s0, -8($fp)
	lw		$ra, -4($fp)
	la		$sp, 4($fp)
	lw		$fp, ($fp)
	jr		$ra


####################################
# drawGrid() ... display current grid[][] matrix
# .TEXT <drawGrid>
	.text
drawGrid:

# Frame:	$fp, $ra, $s0, $s1, $t1
# Uses: 	$s0, $s1
# Clobbers:	$t1

# Locals:
#	- `row' in $s0
#	- `col' in $s1
#	- `&grid[row][col]' in $t1

# Code:
	# set up stack frame
	sw		$fp, -4($sp)
	la		$fp, -4($sp)
	sw		$ra, -4($fp)
	sw		$s0, -8($fp)
	sw		$s1, -12($fp)
	addi	$sp, $sp, -16# $4, $0, $18    

	la		$t1, grid	# load grid[row][col]
	lb		$t3, newLine# load newLine = '\n'
	li		$t4, 1		# sizeOfByte = 1
	li 		$s0, 0		# int row = 0

	drawGridRowLoop:
		beq	$s0, 20, endDrawGridRowLoop			# if row == nRows
		li		$s1, 0										# int col = 0
		drawGridColLoop:
			beq	$s1, 40, endDrawGridColLoop		# if col == nCols
			mul	$t0, $s0, 40							# int index = row * nCols
			add	$t0, $t0, $s1							# index += col
			mul	$t0, $t0, $t4							# index *= sizeOfInt
			add	$t0, $t0, $t1							# index += grid[0][0]
			lb		$a0, ($t0)								# to print %c at grid[row][col]
			li		$v0, 11									# set system call to print
			syscall											# print
			addi	$s1, $s1, 1								# col++
			j 		drawGridColLoop
		endDrawGridColLoop:
			addi 	$s0, $s0, 1								# row++
			lb 	$a0, newLine							# to print '\n'
			li		$v0, 11									# set system call to print
			syscall											# print
			j		drawGridRowLoop

	endDrawGridRowLoop:
	# tear down stack frame
	lw		$s1, -12($fp)
	lw		$s0, -8($fp)
	lw		$ra, -4($fp)
	la		$sp, 4($fp)
	lw		$fp, ($fp)
	jr		$ra


####################################
# initWorm(col,row,len) ... set the wormCol[] and wormRow[]
#    arrays for a worm with head at (row,col) and body segements
#    on the same row and heading to the right (higher col values)
# .TEXT <initWorm>
	.text
initWorm:

# Frame:	$fp, $ra
# Uses: 	$a0, $a1, $a2, $t0, $t1, $t2
# Clobbers:	$t0, $t1, $t2

# Locals:
#	- `col' in $a0
#	- `row' in $a1
#	- `len' in $a2
#	- `newCol' in $t0
#	- `nsegs' in $t1
#	- temporary in $t2

# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	sw	$ra, -8($sp)
	la	$fp, -4($sp)
	addiu	$sp, $sp, -8
	
	add	$t0, $a0, 1			# newCol = col + 1
	li		$t1, 1 				# nsegs = 1
	li		$t2, 0				# temporary = 0
	la		$t3, wormRow		# load wormRow[0]
	la		$t5, wormCol		# load wormCol[0]
	li		$t4, 4				# sizeOfInt = 4
	sw		$a0, wormCol($0)	# wormCol[0] = col
	sw		$a1, wormRow($0)	# wormRow[0] = row

	initWormLoop:
		beq	$t1, $a2, endInitWormLoop	# if nsegs == len
		beq	$t1, 40, endInitWormLoop	# if nsegs == len
		mul 	$t2, $t1, $t4					# temporary = nsegs * sizeOfInt
		add	$t2, $t2, $t5					# temporary = wormCol[0] + nsegs = wormCol[nsegs]
		addi	$t0, $t0, 1						# newCol++
		sw		$t0, ($t2)						# wormCol[nsegs] = newCol++
		mul 	$t2, $t1, $t4					# temporary = nsegs * sizeOfInt
		add	$t2, $t2, $t3					# temporary += wormRow[0]
		sw		$a1, ($t2)						# wormRow[nsegs] = row
		addi	$t1, $t1, 1						# nsegs++
		j		initWormLoop
	endInitWormLoop:
### TODO: Your code goes here

	# tear down stack frame
	lw	$ra, -4($fp)
	la 	$sp, 4($fp)
	lw 	$fp, ($fp)
	jr 	$ra


####################################
# 
#(col,row) ... checks whether (row,col)
#    is a valid coordinate for the grid[][] matrix
# .TEXT <onGrid>
	.text
onGrid:

# Frame:	$fp, $ra
# Uses: 	$a0, $a1, $v0
# Clobbers:	$v0

# Locals:
#	- `col' in $a0
#	- `row' in $a1

# Code:

### TODO: complete this function

	# set up stack frame
	sw    $fp, -4($sp)       # push $fp onto stack
   	la    $fp, -4($sp)       # set up $fp for this function
	sw    $ra, -4($fp)       # save return address
	sw    $s0, -8($fp)       # save $s0 to use as ... int n;
	sw    $s1, -12($fp)      # save $s1 to use as ... int i;
	sw    $s2, -16($fp)      # save $s2 to use as ... int fac;
	addi  $sp, $sp, -20      # reset $sp to last pushed item
    # code for function
	
	li		$v0, 1

	blt	$a0, 0, onGridFalse1		# if col >= 0, continue
	j		ongridStackReset

	onGridFalse1:
		bgt	$a0, 40, onGridFalse2		# if col < NCOLS, continue
		j		ongridStackReset
	onGridFalse2:
		blt	$a1, 0, onGridFalse3		# if row >= 0, continue
		j		ongridStackReset
	onGridFalse3:
		bgt	$a1, 20, onGridFalse4		# if col < NROWS, continue
		j		ongridStackReset
	onGridFalse4:
		li		$v0, 0
		j		ongridStackReset

	ongridStackReset:
	# tear down stack frame
	lw    $s2, -16($fp)      # restore $s2 value
	lw    $s1, -12($fp)      # restore $s1 value
	lw    $s0, -8($fp)       # restore $s0 value
	lw    $ra, -4($fp)       # restore $ra for return
	la    $sp, 4($fp)        # restore $sp (remove stack frame)
	lw    $fp, ($fp)          # restore $fp (remove stack frame)

	jr    $ra                # return 0


####################################
# overlaps(r,c,len) ... checks whether (r,c) holds a body segment
# .TEXT <overlaps>
	.text
overlaps:

# Frame:	$fp, $ra
# Uses: 	$a0, $a1, $a2
# Clobbers:	$t6, $t7

# Locals:
#	- `col' in $a0
#	- `row' in $a1
#	- `len' in $a2
#	- `i' in $t6

# Code:

### TODO: complete this function

	# set up stack frame
	sw    $fp, -4($sp)       # push $fp onto stack
	la    $fp, -4($sp)       # set up $fp for this function
	sw    $ra, -4($fp)       # save return address
	addi  $sp, $sp, -8      # reset $sp to last pushed item

	# code for function
	li		$t6, 0					# int i = 0
	la		$t0, wormCol			# load wormCol
	la		$t1, wormRow			# load wormRow
	li		$t4, 4					# sizeOfInt = 4
	overlapsLoop:
		beq	$t6, $a2, endOverlapLoop		# if i == len
		mul	$t7, $t6, $t4						# t7 = i * 4
		add	$t2, $t7, $t0						# t2 is wormCol[i]
		add	$t3, $t7, $t1						# t3 is wormRow[i]
		bne	$t2, $a0, overlapsLoopIntrv	# wormCol[i] != col: loop back
		bne	$t3, $a1, overlapsLoopIntrv	# wormRow[i] != row: loop back
		li		$v0, 1								# return 1
		j		overlapsLoopStackReset
	overlapsLoopIntrv:
		addi	$t6, $t6, 1							# i++
		j		overlapsLoop
	endOverlapLoop:		
		li		$v0, 0								# return 0

	# tear down stack frame
	overlapsLoopStackReset:
		lw    $ra, -4($fp)       	# restore $ra for return
		la    $sp, 4($fp)        	# restore $sp (remove stack frame)
		lw    $fp, ($fp)          	# restore $fp (remove stack frame)
		jr		$ra
	

####################################
# moveWorm() ... work out new location for head
#         and then move body segments to follow
# updates wormRow[] and wormCol[] arrays

# (col,row) coords of possible places for segments
# done as global data; putting on stack is too messy
	.data
	.align 4
possibleCol: .space 8 * 4	# sizeof(word)
possibleRow: .space 8 * 4	# sizeof(word)

# .TEXT <moveWorm>
	.text
moveWorm:

# Frame:	$fp, $ra, $s0, $s1, $s2, $s3, $s4, $s5, $s6, $s7
# Uses: 	$s0, $s1, $s2, $s3, $s4, $s5, $s6, $s7, $t0, $t1, $t2, $t3
# Clobbers:	$t0, $t1, $t2, $t3

# Locals:
#	- `col' in $s0
#	- `row' in $s1
#	- `len' in $s2
#	- `dx' in $s3
#	- `dy' in $s4
#	- `n' in $s7
#	- `i' in $t0
#	- tmp in $t1
#	- tmp in $t2
#	- tmp in $t3
# 	- `&possibleCol[0]' in $s5
#	- `&possibleRow[0]' in $s6

# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	la	$fp, -4($sp)
	sw	$ra, -8($sp)
	sw	$s0, -12($sp)			# s0 = col
	sw	$s1, -16($sp)			# s1 = row
	sw	$s2, -20($sp)			# s2 = len
	sw	$s3, -24($sp)			# s3 = dx
	sw	$s4, -28($sp)			# s4 = dy
	sw	$s5, -32($sp)			# s5 = possibleCol
	sw	$s6, -36($sp)			# s6 = possibleRow
	sw	$s7, -40($sp)			# s7 = n
	addi	$sp, $sp, -40

### TODO: Your code goes here

	li		$s7, 0				# int n = 0
	li		$s3, -1				# int dx = -1
	lb		$t1, wormCol($0)	# t1 = wormCol[0]
	lb		$t2, wormRow($0)	# t2 = wormRow[0]
	la		$s5, possibleCol	# load possibleCol
	la		$s6, possibleRow	# load possibleRow
	
	moveWormLoop1:
		beq	$s3, 1, endMoveWormLoop1	# if dx == 1, end loop
		li		$s4, -1							# int dy = -1
		moveWormLoop1Nested:
			beq	$s4, 1, endMoveWormLoopNested1	# if dy == 1, end loop
			add	$s0, $t1, $s3				# col = wormCol[0] + dx
			add	$s1, $t2, $s4				# row = wormRow[0] + dy

			move	$a0, $s0						# set onGrid arg[0] as col
			move	$a1, $s1						# set onGrid arg[1] as row
			jal	onGrid						# call onGrid func
			lb		$t1, wormCol($0)			# load wormCol
			lb		$t2, wormRow($0)			# load wormRow
			move	$t3, $v0						# t3 is onGrid return (0 or 1)
			bne	$t3, 1, contMoveWormLoop1Nested		# if t3 != true, continue

			move	$a0, $s0						# set overlaps arg[0] as col
			move	$a1, $s1						# set overlaps arg[1] as row
			move	$a2, $s2						# set overlaps arg[2] as len
			jal	overlaps						# call overlaps func
			lb		$t1, wormCol($0)			# load wormCol
			lb		$t2, wormRow($0)			# load wormRow
			move	$t3, $v0						# t3 is overlaps return (0 or 1)
			bne	$t3, 0, contMoveWormLoop1Nested		# if t3 != false, continue

			li		$t4, 4						# sizeOfInt = 4
			mul	$t4, $s7, $t4				# index = n * 4
			add	$t4, $s5, $t4				# t4 = possibleCol[0] + n = possibleCol[n]
			sw		$s0, ($t4)					# possibleCol[n] = col

			li		$t4, 4						# sizeOfInt = 4
			mul	$t4, $s7, $t4				# index = n * 4
			add	$t4, $s6, $t4				# t4 = possibleRow[0] + n = possibleRow[n]
			sw		$s1, ($t4)					# possibleRow[n] = row

			addi	$s7, $s7, 1					# n++
			j		contMoveWormLoop1Nested
	endMoveWormLoopNested1:
		addi	$s3, $s3, 1						# dx++
		j		moveWormLoop1
	
	contMoveWormLoop1Nested:
		addi	$s4, $s4, 1						# dy++
		j		moveWormLoop1Nested

	endMoveWormLoop1:
		beq	$s7, 0, moveWormReturn0		# if n == 0, return 0

	add	$t0, $s2, -1						# int i = len - 1
	li		$t4, 4								# sizeOfInt = 4
	la		$t1, wormCol						# load wormCol
	la		$t2, wormRow						# load wormRow
	moveWormLoop2:
		beq	$t0, 0, endMoveWormLoop2	# if i = 0, end loop
		blt	$t0, 0, endMoveWormLoop2
		mul	$t6, $t0, $t4					# t6 = i * 4
		add	$t3, $t1, $t6					# t3 = wormCol[0] + i = wormCol[i]
		add	$t5, $t0, -1					# t5 = i - 1
		mul	$t5, $t5, $t4					# t5 *= 4
		add	$t5, $t1, $t5					# t5 = wormCol[0] + (i-1) = wormCol[i-1]
		sw		$t5, ($t3)						# wormCol[i] = wormCol[i-1]

		add	$t3, $t2, $t6					# t3 = wormRow[0] + i = wormRow[i]
		add	$t5, $t0, -1					# t5 = i - 1
		mul	$t5, $t5, $t4					# t5 *= 4
		add	$t5, $t2, $t5					# t5 = wormRow[0] + (i-1) = wormRow[i-1]
		sw		$t5, ($t3)						# wormRow[i] = wormRow[i-1]

		addi	$t0, $t0, -1					# i--
		j		moveWormLoop2

	endMoveWormLoop2:
		move	$a0, $s7							# load n into arg0
		jal	randValue						# prepare for random int
		move	$t0, $v0							# i = randValue(n)

		li		$t4, 4							# sizeOfInt = 4
		mul	$t0, $t0, $t4					# index = i * 4

		la		$t1, wormCol					# load wormCol
		la		$t2, wormRow					# load wormRow

		add	$t3 , $s5, $t0					# t3 = possibleCol[0] + i = possibleCol[i]
		sw		$t3, ($t1)						# wormCol[0] = possibleCol[i]

		add	$t3 , $s6, $t0					# t3 = possibleRow[0] + i = possibleRow[i]
		sw		$t3, ($t2)						# wormRow[0] = possibleRow[i]
		li		$v0, 1
		j		moveWormStackReset
	moveWormReturn0:
		li		$v0, 0							# return 0
		j		moveWormStackReset

	moveWormStackReset:
	# tear down stack frame
	lw	$s7, -36($fp)
	lw	$s6, -32($fp)
	lw	$s5, -28($fp)
	lw	$s4, -24($fp)
	lw	$s3, -20($fp)
	lw	$s2, -16($fp)
	lw	$s1, -12($fp)
	lw	$s0, -8($fp)
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)

	jr	$ra

####################################
# addWormTogrid(N) ... add N worm segments to grid[][] matrix
#    0'th segment is head, located at (wormRow[0],wormCol[0])
#    i'th segment located at (wormRow[i],wormCol[i]), for i > 0
# .TEXT <addWormToGrid>
	.text
addWormToGrid:

# Frame:	$fp, $ra, $s0, $s1, $s2, $s3
# Uses: 	$a0, $s0, $s1, $s2, $s3, $t1
# Clobbers:	$t1

# Locals:
#	- `len' in $a0
#	- `&wormCol[i]' in $s0
#	- `&wormRow[i]' in $s1
#	- `grid[row][col]'
#	- `i' in $t0

# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	la	$fp, -4($sp)
	sw	$ra, -8($sp)
	sw	$s0, -12($sp)
	sw	$s1, -16($sp)
	sw	$s2, -20($sp)
	sw	$s3, -24($sp)
	addi	$sp, $sp, -24

	li		$t4, 1							# sizeOfByte = 1
	lb 	$t6, atSign						# t6 = '@'

### TODO: your code goes here
	lb		$t1, wormRow($0)				# int row = wormRow[0]
	lb		$t2, wormCol($0)				# int col = wormCol[0]
	la		$t3, grid						# load grid[0][0]
	mul	$t5, $t1, 40					# t5 = row * NCOLS
	add	$t5, $t5, $t2					# t5 += col
	mul	$t5, $t5, $t4					# t5 *= sizeOfInt
	add	$t5, $t5, $t3					# t5 += grid[0][0]
	sb		$t6, ($t5)						# grid[row][col] = '@'
	
	li		$t0, 1							# int i = 1
	li		$t6, 0x6F						# t6 = 'o'
	addWormLoop:
		beq	$t0, $a0, endAddWormLoop	# if i == len, end loop
		#blt	$t0, $a0, endAddWormLoop
		li		$t4, 4							# sizeOfInt = 4
		mul	$t7, $t0, $t4					# i *= sizeOfInt
		lw		$t1, wormRow($t7)				# row = wormRow[i]
		#add	$t1, $t1, $t0					# row = row + i = wormRow[i]
		lw		$t2, wormCol($t7)				# col = wormCol[i]
		#add	$t2, $t2, $t0					# col = col + i = wormCol[i]
		mul	$t5, $t1, 40					# t5 = row * NCOLS
		add	$t5, $t5, $t2					# t5 += col
		li		$t4, 1							# sizeOfByte = 1
		mul	$t5, $t5, $t4					# t5 *= sizeOfInt
		add	$t5, $t5, $t3					# t5 += grid[0][0] = grid[row][col]
		sb		$t6, ($t5)						# grid[row][col] = 'o'
		add	$t0, $t0, 1						# i++
		j		addWormLoop
		
	endAddWormLoop:
	# tear down stack frame
	lw	$s3, -20($fp)
	lw	$s2, -16($fp)
	lw	$s1, -12($fp)
	lw	$s0, -8($fp)
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)
	jr	$ra

####################################
# giveUp(msg) ... print error message and exit
# .TEXT <giveUp>
	.text
giveUp:

# Frame:	frameless; divergent
# Uses: 	$a0, $a1
# Clobbers:	$s0, $s1

# Locals:
#	- `progName' in $a0/$s0
#	- `errmsg' in $a1/$s1

# Code:
	add	$s0, $0, $a0
	add	$s1, $0, $a1

	# if (errmsg != NULL) printf("%s\n",errmsg);
	beq	$s1, $0, giveUp_usage

	# puts $a0
	add	$a0, $0, $s1
	addiu	$v0, $0, 4	# print_string
	syscall

	# putchar '\n'
	add	$a0, $0, 0x0a
	addiu	$v0, $0, 11	# print_char
	syscall

giveUp_usage:
	# printf("Usage: %s #Segments #Moves Seed\n", progName);
	la	$a0, giveUp__0
	addiu	$v0, $0, 4	# print_string
	syscall

	add	$a0, $0, $s0
	addiu	$v0, $0, 4	# print_string
	syscall

	la	$a0, giveUp__1
	addiu	$v0, $0, 4	# print_string
	syscall

	# exit(EXIT_FAILURE);
	addi	$a0, $0, 1 # EXIT_FAILURE
	addiu	$v0, $0, 17	# exit2
	syscall
	# doesn't return

####################################
# intValue(str) ... convert string of digits to int value
# .TEXT <intValue>
	.text
intValue:

# Frame:	$fp, $ra
# Uses: 	$t0, $t1, $t2, $t3, $t4, $t5
# Clobbers:	$t0, $t1, $t2, $t3, $t4, $t5

# Locals:
#	- `s' in $t0
#	- `*s' in $t1
#	- `val' in $v0
#	- various temporaries in $t2

# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	sw	$ra, -8($sp)
	la	$fp, -4($sp)
	addiu	$sp, $sp, -8

	# int val = 0;
	add	$v0, $0, $0

	# register various useful values
	addi	$t2, $0, 0x20 # ' '
	addi	$t3, $0, 0x30 # '0'
	addi	$t4, $0, 0x39 # '9'
	addi	$t5, $0, 10

	# for (char *s = str; *s != '\0'; s++) {
intValue_s_init:
	# char *s = str;
	add	$t0, $0, $a0
intValue_s_cond:
	# *s != '\0'
	lb	$t1, ($t0)
	beq	$t1, $0, intValue_s_end

	# if (*s == ' ') continue; # ignore spaces
	beq	$t1, $t2, intValue_s_step

	# if (*s < '0' || *s > '9') return -1;
	blt	$t1, $t3, intValue_isndigit
	bgt	$t1, $t4, intValue_isndigit

	# val = val * 10
	mult	$v0, $t5
	mflo	$v0

	# val = val + (*s - '0');
	sub	$t1, $t1, $t3
	add	$v0, $v0, $t1

intValue_s_step:
	# s = s + 1
	addi	$t0, $t0, 1	# sizeof(byte)
	j	intValue_s_cond
intValue_s_end:

intValue__post:
	# tear down stack frame
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)
	jr	$ra

intValue_isndigit:
	# return -1
	addi	$v0, $0, -1
	j	intValue__post

####################################
# delay(N) ... waste some time; larger N wastes more time
#                            makes the animation believable
# .TEXT <delay>
	.text
delay:

# Frame:	$fp, $ra
# Uses: 	$a0
# Clobbers:	$t0, $t1, $t2

# Locals:
#	- `n' in $a0
#	- `x' in $f6
#	- `i' in $t0
#	- `j' in $t1
#	- `k' in $t2

# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	sw	$ra, -8($sp)
	la	$fp, -4($sp)
	addiu	$sp, $sp, -8

### TODO: your code goes here

	# tear down stack frame
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)
	jr	$ra


####################################
# seedRand(Seed) ... seed the random number generator
# .TEXT <seedRand>
	.text
seedRand:

# Frame:	$fp, $ra
# Uses: 	$a0
# Clobbers:	[none]

# Locals:
#	- `seed' in $a0

# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	sw	$ra, -8($sp)
	la	$fp, -4($sp)
	addiu	$sp, $sp, -8

	# randSeed <- $a0
	sw	$a0, randSeed

seedRand__post:
	# tear down stack frame
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)
	jr	$ra

####################################
# randValue(n) ... generate random value in range 0..n-1
# .TEXT <randValue>
	.text
randValue:

# Frame:	$fp, $ra
# Uses: 	$a0
# Clobbers:	$t0, $t1

# Locals:	[none]
#	- `n' in $a0

# Structure:
#	rand
#	-> [prologue]
#       no intermediate control structures
#	-> [epilogue]

# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	sw	$ra, -8($sp)
	la	$fp, -4($sp)
	addiu	$sp, $sp, -8

	# $t0 <- randSeed
	lw	$t0, randSeed
	# $t1 <- 1103515245 (magic)
	li	$t1, 0x41c64e6d

	# $t0 <- randSeed * 1103515245
	mult	$t0, $t1
	mflo	$t0

	# $t0 <- $t0 + 12345 (more magic)
	addi	$t0, $t0, 0x3039

	# $t0 <- $t0 & RAND_MAX
	and	$t0, $t0, 0x7fffffff

	# randSeed <- $t0
	sw	$t0, randSeed

	# return (randSeed % n)
	div	$t0, $a0
	mfhi	$v0

rand__post:
	# tear down stack frame
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)
	jr	$ra

