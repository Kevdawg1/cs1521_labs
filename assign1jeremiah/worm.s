#
# COMP1521 18s1 -- Assignment 1 -- Worm on a Plane!
#
# Base code by Jashank Jeremy and Wael Alghamdi
# Tweaked (severely) by John Shepherd
#
# Set your tabstop to 8 to make the formatting decent

# Requires:
#  - [no external symbols]

# Provides:
	.globl	wormCol
	.globl	wormRow
	.globl	grid
	.globl	randSeed

	.globl	main
	.globl	clearGrid
	.globl	drawGrid
	.globl	initWorm
	.globl	onGrid
	.globl	overlaps
	.globl	moveWorm
	.globl	addWormToGrid
	.globl	giveUp
	.globl	intValue
	.globl	delay
	.globl	seedRand
	.globl	randValue

	# Let me use $at, please.
	.set	noat

# The following notation is used to suggest places in
# the program, where you might like to add debugging code
#
# If you see e.g. putc('a'), replace by the three lines
# below, with each x replaced by 'a'
#
# print out a single character
# define putc(x)
# 	addi	$a0, $0, x
# 	addiu	$v0, $0, 11
# 	syscall
#
# print out a word-sized int
# define putw(x)
# 	add 	$a0, $0, x
# 	addiu	$v0, $0, 1
# 	syscall

####################################
# .DATA
	.data

	.align 4
wormCol:	.space	40 * 4
	.align 4
wormRow:	.space	40 * 4
	.align 4
grid:		.space	20 * 40 * 1

randSeed:	.word	0

main__0:	.asciiz "Invalid Length (4..20)"
main__1:	.asciiz "Invalid # Moves (0..99)"
main__2:	.asciiz "Invalid Rand Seed (0..Big)"
main__3:	.asciiz "Iteration "
main__4:	.asciiz "Blocked!\n"

	# ANSI escape sequence for 'clear-screen'
main__clear:	.asciiz "\033[H\033[2J"
# main__clear:	.asciiz "__showpage__\n" # for debugging

giveUp__0:	.asciiz "Usage: "
giveUp__1:	.asciiz " Length #Moves Seed\n"

dot:		.ascii	"."
nL:		.ascii 	"\n"
at:		.ascii "@"
owe:		.ascii "o" 	#the letter 'o' is pronounced owe


####################################
# .TEXT <main>
	.text
main:

# Frame:	$fp, $ra, $s0, $s1, $s2, $s3, $s4
# Uses: 	$a0, $a1, $v0, $s0, $s1, $s2, $s3, $s4
# Clobbers:	$a0, $a1

# Locals:
#	- `argc' in $s0
#	- `argv' in $s1
#	- `length' in $s2
#	- `ntimes' in $s3
#	- `i' in $s4

# Structure:
#	main
#	-> [prologue]
#	-> main_seed
#	  -> main_seed_t
#	  -> main_seed_end
#	-> main_seed_phi
#	-> main_i_init
#	-> main_i_cond
#	   -> main_i_step
#	-> main_i_end
#	-> [epilogue]
#	-> main_giveup_0
#	 | main_giveup_1
#	 | main_giveup_2
#	 | main_giveup_3
#	   -> main_giveup_common

# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	sw	$ra, -8($sp)
	sw	$s0, -12($sp)
	sw	$s1, -16($sp)
	sw	$s2, -20($sp)
	sw	$s3, -24($sp)
	sw	$s4, -28($sp)
	la	$fp, -4($sp)
	addiu	$sp, $sp, -28

	# save argc, argv
	add	$s0, $0, $a0
	add	$s1, $0, $a1

	# if (argc < 3) giveUp(argv[0],NULL);
	slti	$at, $s0, 4
	bne	$at, $0, main_giveup_0

	# length = intValue(argv[1]);
	addi	$a0, $s1, 4	# 1 * sizeof(word)
	lw	$a0, ($a0)	# (char *)$a0 = *(char **)$a0
	jal	intValue

	# if (length < 4 || length >= 40)
	#     giveUp(argv[0], "Invalid Length");
	# $at <- (length < 4) ? 1 : 0
	slti	$at, $v0, 4
	bne	$at, $0, main_giveup_1
	# $at <- (length < 40) ? 1 : 0
	slti	$at, $v0, 40
	beq	$at, $0, main_giveup_1
	# ... okay, save length
	add	$s2, $0, $v0

	# ntimes = intValue(argv[2]);
	addi	$a0, $s1, 8	# 2 * sizeof(word)
	lw	$a0, ($a0)
	jal	intValue

	# if (ntimes < 0 || ntimes >= 100)
	#     giveUp(argv[0], "Invalid # Iterations");
	# $at <- (ntimes < 0) ? 1 : 0
	slti	$at, $v0, 0
	bne	$at, $0, main_giveup_2
	# $at <- (ntimes < 100) ? 1 : 0
	slti	$at, $v0, 100
	beq	$at, $0, main_giveup_2
	# ... okay, save ntimes
	add	$s3, $0, $v0

main_seed:
	# seed = intValue(argv[3]);
	add	$a0, $s1, 12	# 3 * sizeof(word)
	lw	$a0, ($a0)
	jal	intValue

	# if (seed < 0) giveUp(argv[0], "Invalid Rand Seed");
	# $at <- (seed < 0) ? 1 : 0
	slt	$at, $v0, $0
	bne	$at, $0, main_giveup_3

main_seed_phi:
	add	$a0, $0, $v0
	jal	seedRand

	# start worm roughly in middle of grid

	# startCol: initial X-coord of head (X = column)
	# int startCol = 40/2 - length/2;
	addi	$s4, $0, 2
	addi	$a0, $0, 40
	div	$a0, $s4
	mflo	$a0
	# length/2
	div	$s2, $s4
	mflo	$s4
	# 40/2 - length/2
	sub	$a0, $a0, $s4

	# startRow: initial Y-coord of head (Y = row)
	# startRow = 20/2;
	addi	$s4, $0, 2
	addi	$a1, $0, 20
	div	$a1, $s4
	mflo	$a1

	# initWorm($a0=startCol, $a1=startRow, $a2=length)
	add	$a2, $0, $s2
	jal	initWorm

main_i_init:
	# int i = 0;
	add	$s4, $0, $0
main_i_cond:
	# i <= ntimes  ->  ntimes >= i  ->  !(ntimes < i)
	#   ->  $at <- (ntimes < i) ? 1 : 0
	slt	$at, $s3, $s4
	bne	$at, $0, main_i_end

	# clearGrid();
	jal	clearGrid

	# addWormToGrid($a0=length);
	add	$a0, $0, $s2
	jal	addWormToGrid

	# printf(CLEAR)
	la	$a0, main__clear
	addiu	$v0, $0, 4	# print_string
	syscall

	# printf("Iteration ")
	la	$a0, main__3
	addiu	$v0, $0, 4	# print_string
	syscall

	# printf("%d",i)
	add	$a0, $0, $s4
	addiu	$v0, $0, 1	# print_int
	syscall

	# putchar('\n')
	addi	$a0, $0, 0x0a
	addiu	$v0, $0, 11	# print_char
	syscall

	# drawGrid();
	jal	drawGrid

	# Debugging? print worm pos as (r1,c1) (r2,c2) ...

	# if (!moveWorm(length)) {...break}
	add	$a0, $0, $s2
	jal	moveWorm
	bne	$v0, $0, main_moveWorm_phi

	# printf("Blocked!\n")
	la	$a0, main__4
	addiu	$v0, $0, 4	# print_string
	syscall

	# break;
	j	main_i_end

main_moveWorm_phi:
	addi	$a0, $0, 1
	jal	delay

main_i_step:
	addi	$s4, $s4, 1
	j	main_i_cond
main_i_end:

	# exit (EXIT_SUCCESS)
	# ... let's return from main with `EXIT_SUCCESS' instead.
	addi	$v0, $0, 0	# EXIT_SUCCESS

main__post:
	# tear down stack frame
	lw	$s4, -24($fp)
	lw	$s3, -20($fp)
	lw	$s2, -16($fp)
	lw	$s1, -12($fp)
	lw	$s0, -8($fp)
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)
	jr	$ra

main_giveup_0:
	add	$a1, $0, $0	# NULL
	j	main_giveup_common
main_giveup_1:
	la	$a1, main__0	# "Invalid Length"
	j	main_giveup_common
main_giveup_2:
	la	$a1, main__1	# "Invalid # Iterations"
	j	main_giveup_common
main_giveup_3:
	la	$a1, main__2	# "Invalid Rand Seed"
	# fall through
main_giveup_common:
	# giveUp ($a0=argv[0], $a1)
	lw	$a0, ($s1)	# argv[0]
	jal	giveUp		# never returns

####################################
# clearGrid() ... set all grid[][] elements to '.'
# .TEXT <clearGrid>
	.text
clearGrid:

# Frame:	$fp, $ra, $s0, $s1
# Uses: 	$s0, $s1, $t1, $t2
# Clobbers:	$t1, $t2, $t3, $t4

# Locals:
#	- `entry' in $s0
#	- `col' in $s1
#	- `&grid[row][col]' in $t1
#	- '.' in $t2
#       - NROWS = 20 in $t3
#       - NCOLS = 40 in $t4
#       - NEntries = NROWS*NCOLS = 800 in $t5

# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	sw	$ra, -8($sp)
	sw	$s0, -12($sp)
	sw	$s1, -16($sp)
	la	$fp, -4($sp)
	addiu	$sp, $sp, -16

### Your code goes here
        move    $s0, $0         # entry = 0;
        li      $t3, 20         # NROWS = 20;
        li      $t4, 40         # NCOLS = 40;
        lb      $t2, dot        # $t2 = '.';

        #NEntries = NROWS*NCOLS
        mult    $t3, $t4
        mflo    $t5

        # $t1 = grid[0][0]
        la      $t1, grid

CGLoop:
        # if (entry >= NEntries) goto endCGLoop
        beq     $s0, $t5, endCGLoop
        bgt     $s0, $t5, endCGLoop

        # &grid[entry] = &grid + entry;
        la      $t1, grid
        add     $t1, $t1, $s0

        # grid[entry] = '.'
        sb      $t2, ($t1)

        addi    $s0, $s0, 1     # entry++;
        j       CGLoop


endCGLoop:


	# tear down stack frame
	lw	$s1, -12($fp)
	lw	$s0, -8($fp)
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)
	jr	$ra


####################################
# drawGrid() ... display current grid[][] matrix
# .TEXT <drawGrid>
	.text
drawGrid:

# Frame:	$fp, $ra, $s0, $s1, $t1
# Uses: 	$s0, $s1
# Clobbers:	$t1

# Locals:
#	- `row' in $s0
#	- `col' in $s1
#	- `&grid[row][col]' in $t1
#       - row*NCOLS + col in $t2
#       - NROWS = 20 in $t3
#       - NCOLS = 40 in $t4

# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	sw	$ra, -8($sp)
	sw	$s0, -12($sp)
	sw	$s1, -16($sp)
	la	$fp, -4($sp)
	addiu	$sp, $sp, -16

### Your code goes here
        move    $s0, $0         # row = 0;
        move    $s1, $0         # col = 0
        li      $t3, 20         # NROWS = 20;
        li      $t4, 40         # NCOLS = 40;

        # $t1 = &grid[0][0]
        la      $t1, grid

DGRowLoop:
        # if (row >= NROWS) goto endDGRowLoop
        beq     $s0, $t3, endDGRowLoop
        bgt     $s0, $t3, endDGRowLoop

        # col = 0
        move    $s1, $0
DGColLoop:

        # if (col >= NCOLSS) goto endDGColLoop
        beq     $s1, $t4, endDGColLoop
        bgt     $s1, $t4, endDGColLoop


        # &grid[row][column] = &grid + row*NCOLS + col;
        la      $t1, grid
	mul 	$t2, $s0, $t4
        #mult    $s0, $t4
        #mflo    $t2
        add     $t2, $t2, $s1
        add     $t1, $t1, $t2


        #printf("%c",grid[row][column]);
        lb		$a0, ($t1)
        li		$v0, 11
        syscall

        #col++;
        addi	$s1, $s1, 1
	j       DGColLoop

endDGColLoop:

        # printf("\n");
        lb		$a0, nL
        li		$v0, 11
        syscall

        addi    $s0, $s0, 1     # row++;
        j       DGRowLoop


endDGRowLoop:

	# tear down stack frame
	lw	$s1, -12($fp)
	lw	$s0, -8($fp)
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)
	jr	$ra


####################################
# initWorm(col,row,len) ... set the wormCol[] and wormRow[]
#    arrays for a worm with head at (row,col) and body segements
#    on the same row and heading to the right (higher col values)
# .TEXT <initWorm>
	.text
initWorm:

# Frame:	$fp, $ra
# Uses: 	$a0, $a1, $a2, $t0, $t1, $t2
# Clobbers:	$t0, $t1, $t2

# Locals:
#	- `col' in $a0
#	- `row' in $a1
#	- `len' in $a2
#	- `newCol' in $t0
#	- `nsegs' in $t1
#	- temporary in $t2
#	- NCOLS in $t3
#	- temporary in $t4

# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	sw	$ra, -8($sp)
	la	$fp, -4($sp)
	addiu	$sp, $sp, -8

### Your code goes here
	# int nsegs = 1;
	li 	$t1, 1
	# int newcol = col +1;
	addi 	$t0, $a0, 1

	# NCOLS = 40
	li      $t3, 40
	# wormCol[0] = col; wormRow[0] = row;
	sw 	$a0, wormCol
	sw 	$a1, wormRow

InitWormLoop:
	# if nsegs >= len, goto endInitWormLoop
	bge	$t1, $a2, endInitWormLoop

	# if (newCol == NCOLS) goto endInitWormLoop;
	beq 	$t0, $t3, endInitWormLoop

	# $t2 = nsegs * 4
	li 	$t4, 4
	mul 	$t2, $t4, $t1

	#t4 = wormCol[nsegs]
	la 	$t4, wormCol
	add 	$t4, $t4, $t2

	#wormCol[nsegs] = newCol++;
	sw 	$t0, ($t4)
	addi 	$t0, $t0, 1

	#t4 = wormRow[nsegs]
	la 	$t4, wormRow
	add 	$t4, $t4, $t2

	#wormRow[nsegs] = row;
	sw 	$a1, ($t4)
	# nsegs++;
	addi 	$t1, $t1, 1

	j 	InitWormLoop


endInitWormLoop:
	# tear down stack frame
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)
	jr	$ra


####################################
# ongrid(col,row) ... checks whether (row,col)
#    is a valid coordinate for the grid[][] matrix
# .TEXT <onGrid>
	.text
onGrid:

# Frame:	$fp, $ra
# Uses: 	$a0, $a1, $v0
# Clobbers:	$v0, $t0, $t1

# Locals:
#	- `col' in $a0
#	- `row' in $a1
#	- NCOLS in $t0
#	- NROWS in $t1

# Code:

### complete this function

	# set up stack frame
	sw	$fp, -4($sp)
	sw	$ra, -8($sp)
	la	$fp, -4($sp)
	addiu	$sp, $sp, -8

    	# code for function
 	# NCOLS = 40
	li 	$t0, 40
	# NCOLS = 20
	li 	$t1, 20
	# if !(col >= 0 && col < NCOLS && row >= 0 && row < NROWS) goto onGridFalse
	blt 	$a0, $0, onGridFalse
	bge 	$a0, $t0, onGridFalse
	blt 	$a1, $0, onGridFalse
	bge 	$a1, $t1, onGridFalse

onGridTrue:
	# return TRUE
	li 	$v0, 1
	j 	endOnGrid
onGridFalse:
	#return FALSE
	li 	$v0, 0
endOnGrid:
	# tear down stack frame
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)
	jr	$ra


####################################
# overlaps(r,c,len) ... checks whether (r,c) holds a body segment
# .TEXT <overlaps>
	.text
overlaps:

# Frame:	$fp, $ra
# Uses: 	$a0, $a1, $a2
# Clobbers:	$t4, $t5, $t6, $t7, $t8, $v0,

# Locals:
#	- `col' in $a0
#	- `row' in $a1
#	- `len' in $a2
#	- `i' in $t6
#	- &wormCol[i] in $t7
#	- &wormRow[i] in $t8
#	- wormCol[i] in $t5
#	- wormRow[i] in $t4


# Code:

	# set up stack frame
	sw	$fp, -4($sp)
	la 	$fp, -4($sp)
	sw	$ra, -8($sp)
	addi 	$sp, $sp, -8

    	# code for function
	# int i = 0
	li 	$t6, 0
	# default return value is 0
	li 	$v0, 0

	la 	$t7, wormCol
	la 	$t8, wormRow

OverlapLoop:
	# if (i >= len) goto endOverlapLoop
	bge 	$t6, $a2, endOverlapLoop
	# $t5 = wormCol[i];
	lw 	$t5, ($t7)
	# $t4 = wormRow[i];
	lw 	$t4, ($t8)

	# if (wormCol[i] != col) goto iterateOverlapLOOP;
	bne 	$t5, $a0, iterateOverlapLOOP
	# if (wormRow[i] != row) goto iterateOverlapLOOP;
	bne 	$t4, $a1, iterateOverlapLOOP

	# return TRUE if both of the above are equal
	li 	$v0, 1
	j 	endOverlapLoop



iterateOverlapLOOP:
	#i++;
	addi 	$t6, $t6, 1
	addi 	$t7, $t7, 4
	addi 	$t8, $t8, 4

	j 	OverlapLoop

endOverlapLoop:
	# tear down stack frame
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)
	jr	$ra


####################################
# moveWorm() ... work out new location for head
#         and then move body segments to follow
# updates wormRow[] and wormCol[] arrays

# (col,row) coords of possible places for segments
# done as global data; putting on stack is too messy
	.data
	.align 4
possibleCol: .space 8 * 4	# sizeof(word)
possibleRow: .space 8 * 4	# sizeof(word)

# .TEXT <moveWorm>
	.text
moveWorm:

# Frame:	$fp, $ra, $s0, $s1, $s2, $s3, $s4, $s5, $s6, $s7
# Uses: 	$s0, $s1, $s2, $s3, $s4, $s5, $s6, $s7, $t0, $t1, $t2, $t3
# Clobbers:	$t0, $t1, $t2, $t3

# Locals:
#	- `col' in $s0
#	- `row' in $s1
#	- `len' in $s2 (need to be initialised?)
#	- `dx' in $s3
#	- `dy' in $s4
#	- `n' in $s7
#	- `i' in $t0
#	- tmp in $t1
#	- tmp in $t2
#	- tmp in $t3
# 	- `&possibleCol[0]' in $s5
#	- `&possibleRow[0]' in $s6

# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	sw	$ra, -8($sp)
	sw	$s0, -12($sp)
	sw	$s1, -16($sp)
	sw	$s2, -20($sp)
	sw	$s3, -24($sp)
	sw	$s4, -28($sp)
	sw	$s5, -32($sp)
	sw	$s6, -36($sp)
	sw	$s7, -40($sp)
	la	$fp, -4($sp)
	addiu	$sp, $sp, -40

### Your code goes here
	# n = 0;
	li 	$s7, 0
	# int possibleCol[8];
	la	$s5, possibleCol
	# int possibleRow[8];
	la	$s6, possibleRow
	# int dx = -1;
	li 	$s3, -1

	# $s2 = len
	move 	$s2, $a0
MWOuterLoop:
	# if (dx > 1) goto endMWOuterLoop;
	li 	$t1, 1
	bgt 	$s3, $t1, endMWOuterLoop

	# int dy = -1
	li 	$s4, -1
MWInnerLoop:
	# if (dy > 1) goto endMWInnerLoop;
	li 	$t1, 1
	bgt 	$s4, $t1, endMWInnerLoop

	# col = wormCol[0] + dx;
	lw 	$s0, wormCol
	add 	$s0, $s0, $s3

	# row = wormRow[0] + dy;
	lw 	$s1, wormRow
	add 	$s1, $s1, $s4

	# $v0 = onGrid(col,row);
	move 	$a0, $s0
	move 	$a1, $s1
	jal 	onGrid

	# if (!onGrid(col,row)), continue;
	li 	$t1, 1
	bne 	$v0, $t1, iterateMWInnerLoop

	# $v0 = overlaps(col,row,len);
	move 	$a0, $s0
	move 	$a1, $s1
	move 	$a2, $s2
	jal 	overlaps

	# if (overlaps(col,row,len)), continue;
	li 	$t1, 1
	beq 	$v0, $t1, iterateMWInnerLoop

	# possibleCol[n] = col;
	li  	$t1, 4
	mul 	$t1, $s7, $t1
	add 	$t1, $t1, $s5
	sw 	$s0, ($t1)

	# possibleRow[n] = row;
	li  	$t1, 4
	mul 	$t1, $s7, $t1
	add 	$t1, $t1, $s6
	sw 	$s1, ($t1)

	# n++;
	addi 	$s7, 1

iterateMWInnerLoop:
	# dy++;
	addi 	$s4, $s4, 1
	j 	MWInnerLoop

endMWInnerLoop:
	# dx++;
	addi 	$s3, $s3, 1
	j 	MWOuterLoop

endMWOuterLoop:
	# if (n == 0) return 0;
	beq 	$s7, $0, endMWFalse
	# int i = len-1;
	move 	$t0, $s2
	addi 	$t0, $t0, -1

MWLoop2:
	# if i <= 0, break
	bge 	$0, $t0, endMWLoop2

	# wormRow[i] = wormRow[i-1];
	li 	$t1, 4
	mul 	$t1, $t1, $t0
	la 	$t2, wormRow
	add 	$t1, $t1, $t2 	# $t2 = &wormRow[i];
	addi 	$t2, $t1, -4	# $t2 = &wormRow[i-1];
	lw 	$t2, ($t2)
	sw 	$t2, ($t1)

	# wormCol[i] = wormCol[i-1];
	li 	$t1, 4
	mul 	$t1, $t1, $t0
	la 	$t2, wormCol
	add 	$t1, $t1, $t2 	# $t2 = &wormCol[i];
	addi 	$t2, $t1, -4	# $t2 = &wormCol[i-1];
	lw 	$t2, ($t2)
	sw 	$t2, ($t1)


	# i--;
	addi 	$t0, $t0, -1
	j 	MWLoop2
endMWLoop2:
	# i = randValue(n);
	move 	$a0, $s7
	jal randValue
	move 	$t0, $v0

	# wormRow[0] = possibleRow[i];
	li 	$t1, 4
	mul 	$t1, $t1, $t0
	add 	$t1, $t1, $s6
	lw 	$t1, ($t1)
	la 	$t2, wormRow
	sw 	$t1, ($t2)

	# wormCol[0] = possibleCol[i];
	li 	$t1, 4
	mul	$t1, $t1, $t0
	add 	$t1, $t1, $s5
	lw 	$t1, ($t1)
	la 	$t2, wormCol
	sw 	$t1, ($t2)



	# return TRUE;
	li 	$v0, 1
	j 	endMW

endMWFalse:
	# return FALSE;
	li 	$v0, 0

endMW:
	# tear down stack frame
	lw	$s7, -36($fp)
	lw	$s6, -32($fp)
	lw	$s5, -28($fp)
	lw	$s4, -24($fp)
	lw	$s3, -20($fp)
	lw	$s2, -16($fp)
	lw	$s1, -12($fp)
	lw	$s0, -8($fp)
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)

	jr	$ra

####################################
# addWormTogrid(N) ... add N worm segments to grid[][] matrix
#    0'th segment is head, located at (wormRow[0],wormCol[0])
#    i'th segment located at (wormRow[i],wormCol[i]), for i > 0
# .TEXT <addWormToGrid>
	.text
addWormToGrid:

# Frame:	$fp, $ra, $s0, $s1, $s2, $s3
# Uses: 	$a0, $s0, $s1, $s2, $s3, $t1
# Clobbers:	$t1, $t2, $t3, $t4

# Locals:
#	- `len' in $a0
#	- `&wormCol[i]' in $s0
#	- `&wormRow[i]' in $s1
#	- row in $s2
#	- col in $s3
#	- `i' in $t0
# 	- byte to be loaded in $t1
#	- `&grid[row][col]' in $t2
#	- NCOLS in $t3
#	- $t4 used for computation of &grid[row][col]



# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	sw	$ra, -8($sp)
	sw	$s0, -12($sp)
	sw	$s1, -16($sp)
	sw	$s2, -20($sp)
	sw	$s3, -24($sp)
	la	$fp, -4($sp)
	addiu	$sp, $sp, -24

###  your code goes here

	# row = wormRow[0];
	la 	$s1, wormRow
	lw 	$s2, ($s1)
	# col = wormCol[0];
	la 	$s0, wormCol
	lw	$s3, ($s0)

	# $t3 = NCOLS
	li 	$t3, 40

	# $t2 =  &grid[0][0]
	la 	$t2, grid
	# $t4 = row * NCOLS
	mul	$t4, $s2, $t3
	# $t4 = row*NCOLS + col
	add 	$t4, $t4, $s3
	# $t2 = &grid[row][col]
	add 	$t2, $t2, $t4

	# grid[row][col] = '@';
	lb 	$t1, at
	sb 	$t1, ($t2)

	# $t1 = i = 1
	li 	$t0, 1
AWTGLoop:
	# if (i >= len) goto endAWTGLoop;
	bge 	$t0, $a0, endAWTGLoop

	# $s0 = &wormCol[i] (via &wormCol++)
	addi 	$s0, $s0, 4
	# $s3 = col = wormCol[i]
	lw 	$s3, ($s0)

	# $s1 = wormRow[i] (via wormRow++)
	addi 	$s1, $s1, 4
	# $s2 = row = wormRow[i]
	lw 	$s2, ($s1)

	# start grid[row][col] = 'o';
	# $t3 = NCOLS
	li 	$t3, 40

	# $t2 =  &grid[0][0]
	la 	$t2, grid
	# $t4 = row * NCOLS
	mul	$t4, $s2, $t3
	# $t4 = row*NCOLS + col
	add 	$t4, $t4, $s3
	# $t2 = &grid[row][col]
	add 	$t2, $t2, $t4

	# grid[row][col] = 'o';
	lb 	$t1, owe 	# owe refers to the letter 'o'
	sb 	$t1, ($t2)
	# end grid[row][col] = 'o';

	# i++;
	addi 	$t0, $t0, 1

	j 	AWTGLoop


endAWTGLoop:


	# tear down stack frame
	lw	$s3, -20($fp)
	lw	$s2, -16($fp)
	lw	$s1, -12($fp)
	lw	$s0, -8($fp)
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)
	jr	$ra

####################################
# giveUp(msg) ... print error message and exit
# .TEXT <giveUp>
	.text
giveUp:

# Frame:	frameless; divergent
# Uses: 	$a0, $a1
# Clobbers:	$s0, $s1

# Locals:
#	- `progName' in $a0/$s0
#	- `errmsg' in $a1/$s1

# Code:
	add	$s0, $0, $a0
	add	$s1, $0, $a1

	# if (errmsg != NULL) printf("%s\n",errmsg);
	beq	$s1, $0, giveUp_usage

	# puts $a0
	add	$a0, $0, $s1
	addiu	$v0, $0, 4	# print_string
	syscall

	# putchar '\n'
	add	$a0, $0, 0x0a
	addiu	$v0, $0, 11	# print_char
	syscall

giveUp_usage:
	# printf("Usage: %s #Segments #Moves Seed\n", progName);
	la	$a0, giveUp__0
	addiu	$v0, $0, 4	# print_string
	syscall

	add	$a0, $0, $s0
	addiu	$v0, $0, 4	# print_string
	syscall

	la	$a0, giveUp__1
	addiu	$v0, $0, 4	# print_string
	syscall

	# exit(EXIT_FAILURE);
	addi	$a0, $0, 1 # EXIT_FAILURE
	addiu	$v0, $0, 17	# exit2
	syscall
	# doesn't return

####################################
# intValue(str) ... convert string of digits to int value
# .TEXT <intValue>
	.text
intValue:

# Frame:	$fp, $ra
# Uses: 	$t0, $t1, $t2, $t3, $t4, $t5
# Clobbers:	$t0, $t1, $t2, $t3, $t4, $t5

# Locals:
#	- `s' in $t0
#	- `*s' in $t1
#	- `val' in $v0
#	- various temporaries in $t2

# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	sw	$ra, -8($sp)
	la	$fp, -4($sp)
	addiu	$sp, $sp, -8

	# int val = 0;
	add	$v0, $0, $0

	# register various useful values
	addi	$t2, $0, 0x20 # ' '
	addi	$t3, $0, 0x30 # '0'
	addi	$t4, $0, 0x39 # '9'
	addi	$t5, $0, 10

	# for (char *s = str; *s != '\0'; s++) {
intValue_s_init:
	# char *s = str;
	add	$t0, $0, $a0
intValue_s_cond:
	# *s != '\0'
	lb	$t1, ($t0)
	beq	$t1, $0, intValue_s_end

	# if (*s == ' ') continue; # ignore spaces
	beq	$t1, $t2, intValue_s_step

	# if (*s < '0' || *s > '9') return -1;
	blt	$t1, $t3, intValue_isndigit
	bgt	$t1, $t4, intValue_isndigit

	# val = val * 10
	mult	$v0, $t5
	mflo	$v0

	# val = val + (*s - '0');
	sub	$t1, $t1, $t3
	add	$v0, $v0, $t1

intValue_s_step:
	# s = s + 1
	addi	$t0, $t0, 1	# sizeof(byte)
	j	intValue_s_cond
intValue_s_end:

intValue__post:
	# tear down stack frame
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)
	jr	$ra

intValue_isndigit:
	# return -1
	addi	$v0, $0, -1
	j	intValue__post

####################################
# delay(N) ... waste some time; larger N wastes more time
#                            makes the animation believable
# .TEXT <delay>
	.text
delay:

# Frame:	$fp, $ra
# Uses: 	$a0
# Clobbers:	$t0, $t1, $t2, $f6, $f7

# Locals:
#	- `n' in $a0
#	- `x' in $f6
#	- used as a helper: $f7
#	- `i' in $t0
#	- `j' in $t1
#	- `k' in $t2

# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	sw	$ra, -8($sp)
	la	$fp, -4($sp)
	addiu	$sp, $sp, -8

###  your code goes here
	# int x = 3;
	li.s 	$f6, 3.0
	# f7 = 3.0 to multiply
	li.s 	$f7, 3.0
	# int i = 0;
	li 	$t0, 0

DOuterLoop:
	# if (i >= n) break;
	bge 	$t0, $a0, endDOuterLoop

	# int j = 0
	li  	$t1, 0
DMidLoop:
	# if (j >= 40000) break;
	bge 	$t1, 40000, endDMidLoop

	# int k = 0;
DInnerLoop:
	# if (k >= 1000) break;
	bge 	$t1, 40000, endDInnerLoop
	# x = x * 3;
	mul.s 	$f6, $f6, $f7
	# k++;
	addi 	$t2, $t2, 1
endDInnerLoop:
	#j++;
	addi 	$t1, $t1, 1
	j 	DMidLoop
endDMidLoop:
	#i++;
	addi 	$t0, $t0, 1
	j 	DOuterLoop
endDOuterLoop:

	# tear down stack frame
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)
	jr	$ra


####################################
# seedRand(Seed) ... seed the random number generator
# .TEXT <seedRand>
	.text
seedRand:

# Frame:	$fp, $ra
# Uses: 	$a0
# Clobbers:	[none]

# Locals:
#	- `seed' in $a0

# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	sw	$ra, -8($sp)
	la	$fp, -4($sp)
	addiu	$sp, $sp, -8

	# randSeed <- $a0
	sw	$a0, randSeed

seedRand__post:
	# tear down stack frame
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)
	jr	$ra

####################################
# randValue(n) ... generate random value in range 0..n-1
# .TEXT <randValue>
	.text
randValue:

# Frame:	$fp, $ra
# Uses: 	$a0
# Clobbers:	$t0, $t1

# Locals:	[none]
#	- `n' in $a0

# Structure:
#	rand
#	-> [prologue]
#       no intermediate control structures
#	-> [epilogue]

# Code:
	# set up stack frame
	sw	$fp, -4($sp)
	sw	$ra, -8($sp)
	la	$fp, -4($sp)
	addiu	$sp, $sp, -8

	# $t0 <- randSeed
	lw	$t0, randSeed
	# $t1 <- 1103515245 (magic)
	li	$t1, 0x41c64e6d

	# $t0 <- randSeed * 1103515245
	mult	$t0, $t1
	mflo	$t0

	# $t0 <- $t0 + 12345 (more magic)
	addi	$t0, $t0, 0x3039

	# $t0 <- $t0 & RAND_MAX
	and	$t0, $t0, 0x7fffffff

	# randSeed <- $t0
	sw	$t0, randSeed

	# return (randSeed % n)
	div	$t0, $a0
	mfhi	$v0

rand__post:
	# tear down stack frame
	lw	$ra, -4($fp)
	la	$sp, 4($fp)
	lw	$fp, ($fp)
	jr	$ra
